package com.snithik.examapp.service;

import com.snithik.examapp.domain.Answers;
import com.snithik.examapp.repository.AnswersRepository;
import com.snithik.examapp.service.dto.AnswersDTO;
import com.snithik.examapp.service.mapper.AnswersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Answers.
 */
@Service
@Transactional
public class AnswersService {

    private final Logger log = LoggerFactory.getLogger(AnswersService.class);

    private final AnswersRepository answersRepository;

    private final AnswersMapper answersMapper;

    public AnswersService(AnswersRepository answersRepository, AnswersMapper answersMapper) {
        this.answersRepository = answersRepository;
        this.answersMapper = answersMapper;
    }

    /**
     * Save a answers.
     *
     * @param answersDTO the entity to save
     * @return the persisted entity
     */
    public AnswersDTO save(AnswersDTO answersDTO) {
        log.debug("Request to save Answers : {}", answersDTO);
        Answers answers = answersMapper.toEntity(answersDTO);
        answers = answersRepository.save(answers);
        return answersMapper.toDto(answers);
    }

    /**
     * Get all the answers.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AnswersDTO> findAll() {
        log.debug("Request to get all Answers");
        return answersRepository.findAll().stream()
            .map(answersMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one answers by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<AnswersDTO> findOne(Long id) {
        log.debug("Request to get Answers : {}", id);
        return answersRepository.findById(id)
            .map(answersMapper::toDto);
    }

    /**
     * Delete the answers by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Answers : {}", id);
        answersRepository.deleteById(id);
    }
}
