package com.snithik.examapp.service;

import com.snithik.examapp.domain.Contacts;
import com.snithik.examapp.repository.ContactsRepository;
import com.snithik.examapp.service.dto.ContactsDTO;
import com.snithik.examapp.service.mapper.ContactsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Contacts.
 */
@Service
@Transactional
public class ContactsService {

    private final Logger log = LoggerFactory.getLogger(ContactsService.class);

    private final ContactsRepository contactsRepository;

    private final ContactsMapper contactsMapper;

    public ContactsService(ContactsRepository contactsRepository, ContactsMapper contactsMapper) {
        this.contactsRepository = contactsRepository;
        this.contactsMapper = contactsMapper;
    }

    /**
     * Save a contacts.
     *
     * @param contactsDTO the entity to save
     * @return the persisted entity
     */
    public ContactsDTO save(ContactsDTO contactsDTO) {
        log.debug("Request to save Contacts : {}", contactsDTO);
        Contacts contacts = contactsMapper.toEntity(contactsDTO);
        contacts = contactsRepository.save(contacts);
        return contactsMapper.toDto(contacts);
    }

    /**
     * Get all the contacts.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ContactsDTO> findAll() {
        log.debug("Request to get all Contacts");
        return contactsRepository.findAll().stream()
            .map(contactsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one contacts by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ContactsDTO> findOne(Long id) {
        log.debug("Request to get Contacts : {}", id);
        return contactsRepository.findById(id)
            .map(contactsMapper::toDto);
    }

    /**
     * Delete the contacts by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Contacts : {}", id);
        contactsRepository.deleteById(id);
    }
}
