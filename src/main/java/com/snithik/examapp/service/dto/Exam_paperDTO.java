package com.snithik.examapp.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Exam_paper entity.
 */
public class Exam_paperDTO implements Serializable {

    private Long id;

    private String name;

    private LocalDate creation_date;

    private String created_by;

    private LocalDate last_update_date;

    private String last_updated_by;


    private Long collegeidId;

    private Long questionidId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public Long getCollegeidId() {
        return collegeidId;
    }

    public void setCollegeidId(Long collegesId) {
        this.collegeidId = collegesId;
    }

    public Long getQuestionidId() {
        return questionidId;
    }

    public void setQuestionidId(Long questionsId) {
        this.questionidId = questionsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Exam_paperDTO exam_paperDTO = (Exam_paperDTO) o;
        if (exam_paperDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), exam_paperDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Exam_paperDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            ", collegeid=" + getCollegeidId() +
            ", questionid=" + getQuestionidId() +
            "}";
    }
}
