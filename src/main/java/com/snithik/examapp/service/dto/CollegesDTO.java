package com.snithik.examapp.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Colleges entity.
 */
public class CollegesDTO implements Serializable {

    private Long id;

    private String college_name;

    private String college_code;

    private String affiliated_university;

    private String address;

    private String address2;

    private String city;

    private String state;

    private String country;

    private String zip_code;

    private Long contact_number;

    private String email;

    private Boolean status;

    private LocalDate creation_date;

    private String created_by;

    private LocalDate last_update_date;

    private String last_update_by;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    public String getCollege_code() {
        return college_code;
    }

    public void setCollege_code(String college_code) {
        this.college_code = college_code;
    }

    public String getAffiliated_university() {
        return affiliated_university;
    }

    public void setAffiliated_university(String affiliated_university) {
        this.affiliated_university = affiliated_university;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public Long getContact_number() {
        return contact_number;
    }

    public void setContact_number(Long contact_number) {
        this.contact_number = contact_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_update_by() {
        return last_update_by;
    }

    public void setLast_update_by(String last_update_by) {
        this.last_update_by = last_update_by;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CollegesDTO collegesDTO = (CollegesDTO) o;
        if (collegesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), collegesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CollegesDTO{" +
            "id=" + getId() +
            ", college_name='" + getCollege_name() + "'" +
            ", college_code='" + getCollege_code() + "'" +
            ", affiliated_university='" + getAffiliated_university() + "'" +
            ", address='" + getAddress() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", zip_code='" + getZip_code() + "'" +
            ", contact_number=" + getContact_number() +
            ", email='" + getEmail() + "'" +
            ", status='" + isStatus() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_update_by='" + getLast_update_by() + "'" +
            "}";
    }
}
