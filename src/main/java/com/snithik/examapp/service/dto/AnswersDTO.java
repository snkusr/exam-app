package com.snithik.examapp.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Answers entity.
 */
public class AnswersDTO implements Serializable {

    private Long id;

    private String answer;

    private LocalDate answered_date;

    private LocalDate end_time;

    private LocalDate start_time;

    private LocalDate creation_date;

    private String created_by;

    private LocalDate last_update_date;

    private LocalDate last_updated_by;


    private Long question_idId;

    private Long studentidId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public LocalDate getAnswered_date() {
        return answered_date;
    }

    public void setAnswered_date(LocalDate answered_date) {
        this.answered_date = answered_date;
    }

    public LocalDate getEnd_time() {
        return end_time;
    }

    public void setEnd_time(LocalDate end_time) {
        this.end_time = end_time;
    }

    public LocalDate getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDate start_time) {
        this.start_time = start_time;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public LocalDate getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_updated_by(LocalDate last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public Long getQuestion_idId() {
        return question_idId;
    }

    public void setQuestion_idId(Long questionsId) {
        this.question_idId = questionsId;
    }

    public Long getStudentidId() {
        return studentidId;
    }

    public void setStudentidId(Long studentsId) {
        this.studentidId = studentsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AnswersDTO answersDTO = (AnswersDTO) o;
        if (answersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), answersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AnswersDTO{" +
            "id=" + getId() +
            ", answer='" + getAnswer() + "'" +
            ", answered_date='" + getAnswered_date() + "'" +
            ", end_time='" + getEnd_time() + "'" +
            ", start_time='" + getStart_time() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            ", question_id=" + getQuestion_idId() +
            ", studentid=" + getStudentidId() +
            "}";
    }
}
