package com.snithik.examapp.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Contacts entity.
 */
public class ContactsDTO implements Serializable {

    private Long id;

    private String contact_name;

    private Long mobile;

    private String email;

    private String designation;

    private Boolean status;

    private LocalDate creation_date;

    private String created_by;

    private LocalDate last_update_date;

    private String last_updated_by;


    private Long collegeidId;

    private Long departmentidId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public Long getCollegeidId() {
        return collegeidId;
    }

    public void setCollegeidId(Long collegesId) {
        this.collegeidId = collegesId;
    }

    public Long getDepartmentidId() {
        return departmentidId;
    }

    public void setDepartmentidId(Long departmentsId) {
        this.departmentidId = departmentsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContactsDTO contactsDTO = (ContactsDTO) o;
        if (contactsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contactsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContactsDTO{" +
            "id=" + getId() +
            ", contact_name='" + getContact_name() + "'" +
            ", mobile=" + getMobile() +
            ", email='" + getEmail() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", status='" + isStatus() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            ", collegeid=" + getCollegeidId() +
            ", departmentid=" + getDepartmentidId() +
            "}";
    }
}
