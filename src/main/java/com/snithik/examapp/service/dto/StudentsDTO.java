package com.snithik.examapp.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Students entity.
 */
public class StudentsDTO implements Serializable {

    private Long id;

    private String student_name;

    private Long mobile;

    private String email;

    private String studying_year;

    private Boolean status;

    private LocalDate creation_date;

    private String created_by;

    private LocalDate last_update_date;

    private String last_updated_by;

    private String department;

    private Long emcetecet_rank;

    private Float current_CGPA;

    private String fathers_occupation;

    private String mothers_occupation;

    private Integer siblings;

    private String family_income;

    private String native_place;

    private String languages_known;


    private Long collegeidId;

    private Long departmentidId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStudying_year() {
        return studying_year;
    }

    public void setStudying_year(String studying_year) {
        this.studying_year = studying_year;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Long getEmcetecet_rank() {
        return emcetecet_rank;
    }

    public void setEmcetecet_rank(Long emcetecet_rank) {
        this.emcetecet_rank = emcetecet_rank;
    }

    public Float getCurrent_CGPA() {
        return current_CGPA;
    }

    public void setCurrent_CGPA(Float current_CGPA) {
        this.current_CGPA = current_CGPA;
    }

    public String getFathers_occupation() {
        return fathers_occupation;
    }

    public void setFathers_occupation(String fathers_occupation) {
        this.fathers_occupation = fathers_occupation;
    }

    public String getMothers_occupation() {
        return mothers_occupation;
    }

    public void setMothers_occupation(String mothers_occupation) {
        this.mothers_occupation = mothers_occupation;
    }

    public Integer getSiblings() {
        return siblings;
    }

    public void setSiblings(Integer siblings) {
        this.siblings = siblings;
    }

    public String getFamily_income() {
        return family_income;
    }

    public void setFamily_income(String family_income) {
        this.family_income = family_income;
    }

    public String getNative_place() {
        return native_place;
    }

    public void setNative_place(String native_place) {
        this.native_place = native_place;
    }

    public String getLanguages_known() {
        return languages_known;
    }

    public void setLanguages_known(String languages_known) {
        this.languages_known = languages_known;
    }

    public Long getCollegeidId() {
        return collegeidId;
    }

    public void setCollegeidId(Long collegesId) {
        this.collegeidId = collegesId;
    }

    public Long getDepartmentidId() {
        return departmentidId;
    }

    public void setDepartmentidId(Long departmentsId) {
        this.departmentidId = departmentsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StudentsDTO studentsDTO = (StudentsDTO) o;
        if (studentsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), studentsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StudentsDTO{" +
            "id=" + getId() +
            ", student_name='" + getStudent_name() + "'" +
            ", mobile=" + getMobile() +
            ", email='" + getEmail() + "'" +
            ", studying_year='" + getStudying_year() + "'" +
            ", status='" + isStatus() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            ", department='" + getDepartment() + "'" +
            ", emcetecet_rank=" + getEmcetecet_rank() +
            ", current_CGPA=" + getCurrent_CGPA() +
            ", fathers_occupation='" + getFathers_occupation() + "'" +
            ", mothers_occupation='" + getMothers_occupation() + "'" +
            ", siblings=" + getSiblings() +
            ", family_income='" + getFamily_income() + "'" +
            ", native_place='" + getNative_place() + "'" +
            ", languages_known='" + getLanguages_known() + "'" +
            ", collegeid=" + getCollegeidId() +
            ", departmentid=" + getDepartmentidId() +
            "}";
    }
}
