package com.snithik.examapp.service;

import com.snithik.examapp.domain.Colleges;
import com.snithik.examapp.repository.CollegesRepository;
import com.snithik.examapp.service.dto.CollegesDTO;
import com.snithik.examapp.service.mapper.CollegesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Colleges.
 */
@Service
@Transactional
public class CollegesService {

    private final Logger log = LoggerFactory.getLogger(CollegesService.class);

    private final CollegesRepository collegesRepository;

    private final CollegesMapper collegesMapper;

    public CollegesService(CollegesRepository collegesRepository, CollegesMapper collegesMapper) {
        this.collegesRepository = collegesRepository;
        this.collegesMapper = collegesMapper;
    }

    /**
     * Save a colleges.
     *
     * @param collegesDTO the entity to save
     * @return the persisted entity
     */
    public CollegesDTO save(CollegesDTO collegesDTO) {
        log.debug("Request to save Colleges : {}", collegesDTO);
        Colleges colleges = collegesMapper.toEntity(collegesDTO);
        colleges = collegesRepository.save(colleges);
        return collegesMapper.toDto(colleges);
    }

    /**
     * Get all the colleges.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CollegesDTO> findAll() {
        log.debug("Request to get all Colleges");
        return collegesRepository.findAll().stream()
            .map(collegesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one colleges by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<CollegesDTO> findOne(Long id) {
        log.debug("Request to get Colleges : {}", id);
        return collegesRepository.findById(id)
            .map(collegesMapper::toDto);
    }

    /**
     * Delete the colleges by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Colleges : {}", id);
        collegesRepository.deleteById(id);
    }
}
