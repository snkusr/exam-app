package com.snithik.examapp.service.mapper;

import com.snithik.examapp.domain.*;
import com.snithik.examapp.service.dto.AnswersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Answers and its DTO AnswersDTO.
 */
@Mapper(componentModel = "spring", uses = {QuestionsMapper.class, StudentsMapper.class})
public interface AnswersMapper extends EntityMapper<AnswersDTO, Answers> {

    @Mapping(source = "question_id.id", target = "question_idId")
    @Mapping(source = "studentid.id", target = "studentidId")
    AnswersDTO toDto(Answers answers);

    @Mapping(source = "question_idId", target = "question_id")
    @Mapping(source = "studentidId", target = "studentid")
    Answers toEntity(AnswersDTO answersDTO);

    default Answers fromId(Long id) {
        if (id == null) {
            return null;
        }
        Answers answers = new Answers();
        answers.setId(id);
        return answers;
    }
}
