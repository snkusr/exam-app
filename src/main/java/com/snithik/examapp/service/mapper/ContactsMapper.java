package com.snithik.examapp.service.mapper;

import com.snithik.examapp.domain.*;
import com.snithik.examapp.service.dto.ContactsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Contacts and its DTO ContactsDTO.
 */
@Mapper(componentModel = "spring", uses = {CollegesMapper.class, DepartmentsMapper.class})
public interface ContactsMapper extends EntityMapper<ContactsDTO, Contacts> {

    @Mapping(source = "collegeid.id", target = "collegeidId")
    @Mapping(source = "departmentid.id", target = "departmentidId")
    ContactsDTO toDto(Contacts contacts);

    @Mapping(source = "collegeidId", target = "collegeid")
    @Mapping(source = "departmentidId", target = "departmentid")
    Contacts toEntity(ContactsDTO contactsDTO);

    default Contacts fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contacts contacts = new Contacts();
        contacts.setId(id);
        return contacts;
    }
}
