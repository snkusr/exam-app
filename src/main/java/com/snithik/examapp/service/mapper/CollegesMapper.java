package com.snithik.examapp.service.mapper;

import com.snithik.examapp.domain.*;
import com.snithik.examapp.service.dto.CollegesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Colleges and its DTO CollegesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CollegesMapper extends EntityMapper<CollegesDTO, Colleges> {



    default Colleges fromId(Long id) {
        if (id == null) {
            return null;
        }
        Colleges colleges = new Colleges();
        colleges.setId(id);
        return colleges;
    }
}
