package com.snithik.examapp.service.mapper;

import com.snithik.examapp.domain.*;
import com.snithik.examapp.service.dto.Question_typesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Question_types and its DTO Question_typesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface Question_typesMapper extends EntityMapper<Question_typesDTO, Question_types> {



    default Question_types fromId(Long id) {
        if (id == null) {
            return null;
        }
        Question_types question_types = new Question_types();
        question_types.setId(id);
        return question_types;
    }
}
