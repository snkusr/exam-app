package com.snithik.examapp.service.mapper;

import com.snithik.examapp.domain.*;
import com.snithik.examapp.service.dto.Exam_paperDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Exam_paper and its DTO Exam_paperDTO.
 */
@Mapper(componentModel = "spring", uses = {CollegesMapper.class, QuestionsMapper.class})
public interface Exam_paperMapper extends EntityMapper<Exam_paperDTO, Exam_paper> {

    @Mapping(source = "collegeid.id", target = "collegeidId")
    @Mapping(source = "questionid.id", target = "questionidId")
    Exam_paperDTO toDto(Exam_paper exam_paper);

    @Mapping(source = "collegeidId", target = "collegeid")
    @Mapping(source = "questionidId", target = "questionid")
    Exam_paper toEntity(Exam_paperDTO exam_paperDTO);

    default Exam_paper fromId(Long id) {
        if (id == null) {
            return null;
        }
        Exam_paper exam_paper = new Exam_paper();
        exam_paper.setId(id);
        return exam_paper;
    }
}
