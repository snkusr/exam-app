package com.snithik.examapp.service.mapper;

import com.snithik.examapp.domain.*;
import com.snithik.examapp.service.dto.QuestionsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Questions and its DTO QuestionsDTO.
 */
@Mapper(componentModel = "spring", uses = {Question_typesMapper.class})
public interface QuestionsMapper extends EntityMapper<QuestionsDTO, Questions> {

    @Mapping(source = "questiontypeid.id", target = "questiontypeidId")
    QuestionsDTO toDto(Questions questions);

    @Mapping(source = "questiontypeidId", target = "questiontypeid")
    Questions toEntity(QuestionsDTO questionsDTO);

    default Questions fromId(Long id) {
        if (id == null) {
            return null;
        }
        Questions questions = new Questions();
        questions.setId(id);
        return questions;
    }
}
