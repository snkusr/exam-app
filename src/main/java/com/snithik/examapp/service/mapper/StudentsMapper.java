package com.snithik.examapp.service.mapper;

import com.snithik.examapp.domain.*;
import com.snithik.examapp.service.dto.StudentsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Students and its DTO StudentsDTO.
 */
@Mapper(componentModel = "spring", uses = {CollegesMapper.class, DepartmentsMapper.class})
public interface StudentsMapper extends EntityMapper<StudentsDTO, Students> {

    @Mapping(source = "collegeid.id", target = "collegeidId")
    @Mapping(source = "departmentid.id", target = "departmentidId")
    StudentsDTO toDto(Students students);

    @Mapping(source = "collegeidId", target = "collegeid")
    @Mapping(source = "departmentidId", target = "departmentid")
    Students toEntity(StudentsDTO studentsDTO);

    default Students fromId(Long id) {
        if (id == null) {
            return null;
        }
        Students students = new Students();
        students.setId(id);
        return students;
    }
}
