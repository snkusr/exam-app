package com.snithik.examapp.service;

import com.snithik.examapp.domain.Question_types;
import com.snithik.examapp.repository.Question_typesRepository;
import com.snithik.examapp.service.dto.Question_typesDTO;
import com.snithik.examapp.service.mapper.Question_typesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Question_types.
 */
@Service
@Transactional
public class Question_typesService {

    private final Logger log = LoggerFactory.getLogger(Question_typesService.class);

    private final Question_typesRepository question_typesRepository;

    private final Question_typesMapper question_typesMapper;

    public Question_typesService(Question_typesRepository question_typesRepository, Question_typesMapper question_typesMapper) {
        this.question_typesRepository = question_typesRepository;
        this.question_typesMapper = question_typesMapper;
    }

    /**
     * Save a question_types.
     *
     * @param question_typesDTO the entity to save
     * @return the persisted entity
     */
    public Question_typesDTO save(Question_typesDTO question_typesDTO) {
        log.debug("Request to save Question_types : {}", question_typesDTO);
        Question_types question_types = question_typesMapper.toEntity(question_typesDTO);
        question_types = question_typesRepository.save(question_types);
        return question_typesMapper.toDto(question_types);
    }

    /**
     * Get all the question_types.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Question_typesDTO> findAll() {
        log.debug("Request to get all Question_types");
        return question_typesRepository.findAll().stream()
            .map(question_typesMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one question_types by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Question_typesDTO> findOne(Long id) {
        log.debug("Request to get Question_types : {}", id);
        return question_typesRepository.findById(id)
            .map(question_typesMapper::toDto);
    }

    /**
     * Delete the question_types by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Question_types : {}", id);
        question_typesRepository.deleteById(id);
    }
}
