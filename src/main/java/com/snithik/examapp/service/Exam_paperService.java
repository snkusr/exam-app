package com.snithik.examapp.service;

import com.snithik.examapp.domain.Exam_paper;
import com.snithik.examapp.repository.Exam_paperRepository;
import com.snithik.examapp.service.dto.Exam_paperDTO;
import com.snithik.examapp.service.mapper.Exam_paperMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Exam_paper.
 */
@Service
@Transactional
public class Exam_paperService {

    private final Logger log = LoggerFactory.getLogger(Exam_paperService.class);

    private final Exam_paperRepository exam_paperRepository;

    private final Exam_paperMapper exam_paperMapper;

    public Exam_paperService(Exam_paperRepository exam_paperRepository, Exam_paperMapper exam_paperMapper) {
        this.exam_paperRepository = exam_paperRepository;
        this.exam_paperMapper = exam_paperMapper;
    }

    /**
     * Save a exam_paper.
     *
     * @param exam_paperDTO the entity to save
     * @return the persisted entity
     */
    public Exam_paperDTO save(Exam_paperDTO exam_paperDTO) {
        log.debug("Request to save Exam_paper : {}", exam_paperDTO);
        Exam_paper exam_paper = exam_paperMapper.toEntity(exam_paperDTO);
        exam_paper = exam_paperRepository.save(exam_paper);
        return exam_paperMapper.toDto(exam_paper);
    }

    /**
     * Get all the exam_papers.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Exam_paperDTO> findAll() {
        log.debug("Request to get all Exam_papers");
        return exam_paperRepository.findAll().stream()
            .map(exam_paperMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one exam_paper by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Exam_paperDTO> findOne(Long id) {
        log.debug("Request to get Exam_paper : {}", id);
        return exam_paperRepository.findById(id)
            .map(exam_paperMapper::toDto);
    }

    /**
     * Delete the exam_paper by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Exam_paper : {}", id);
        exam_paperRepository.deleteById(id);
    }
}
