package com.snithik.examapp.web.rest;
import com.snithik.examapp.service.AnswersService;
import com.snithik.examapp.web.rest.errors.BadRequestAlertException;
import com.snithik.examapp.web.rest.util.HeaderUtil;
import com.snithik.examapp.service.dto.AnswersDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Answers.
 */
@RestController
@RequestMapping("/api")
public class AnswersResource {

    private final Logger log = LoggerFactory.getLogger(AnswersResource.class);

    private static final String ENTITY_NAME = "answers";

    private final AnswersService answersService;

    public AnswersResource(AnswersService answersService) {
        this.answersService = answersService;
    }

    /**
     * POST  /answers : Create a new answers.
     *
     * @param answersDTO the answersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new answersDTO, or with status 400 (Bad Request) if the answers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/answers")
    public ResponseEntity<AnswersDTO> createAnswers(@RequestBody AnswersDTO answersDTO) throws URISyntaxException {
        log.debug("REST request to save Answers : {}", answersDTO);
        if (answersDTO.getId() != null) {
            throw new BadRequestAlertException("A new answers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AnswersDTO result = answersService.save(answersDTO);
        return ResponseEntity.created(new URI("/api/answers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /answers : Updates an existing answers.
     *
     * @param answersDTO the answersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated answersDTO,
     * or with status 400 (Bad Request) if the answersDTO is not valid,
     * or with status 500 (Internal Server Error) if the answersDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/answers")
    public ResponseEntity<AnswersDTO> updateAnswers(@RequestBody AnswersDTO answersDTO) throws URISyntaxException {
        log.debug("REST request to update Answers : {}", answersDTO);
        if (answersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AnswersDTO result = answersService.save(answersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, answersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /answers : get all the answers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of answers in body
     */
    @GetMapping("/answers")
    public List<AnswersDTO> getAllAnswers() {
        log.debug("REST request to get all Answers");
        return answersService.findAll();
    }

    /**
     * GET  /answers/:id : get the "id" answers.
     *
     * @param id the id of the answersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the answersDTO, or with status 404 (Not Found)
     */
    @GetMapping("/answers/{id}")
    public ResponseEntity<AnswersDTO> getAnswers(@PathVariable Long id) {
        log.debug("REST request to get Answers : {}", id);
        Optional<AnswersDTO> answersDTO = answersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(answersDTO);
    }

    /**
     * DELETE  /answers/:id : delete the "id" answers.
     *
     * @param id the id of the answersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/answers/{id}")
    public ResponseEntity<Void> deleteAnswers(@PathVariable Long id) {
        log.debug("REST request to delete Answers : {}", id);
        answersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
