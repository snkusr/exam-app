package com.snithik.examapp.web.rest;
import com.snithik.examapp.service.ContactsService;
import com.snithik.examapp.web.rest.errors.BadRequestAlertException;
import com.snithik.examapp.web.rest.util.HeaderUtil;
import com.snithik.examapp.service.dto.ContactsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Contacts.
 */
@RestController
@RequestMapping("/api")
public class ContactsResource {

    private final Logger log = LoggerFactory.getLogger(ContactsResource.class);

    private static final String ENTITY_NAME = "contacts";

    private final ContactsService contactsService;

    public ContactsResource(ContactsService contactsService) {
        this.contactsService = contactsService;
    }

    /**
     * POST  /contacts : Create a new contacts.
     *
     * @param contactsDTO the contactsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contactsDTO, or with status 400 (Bad Request) if the contacts has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/contacts")
    public ResponseEntity<ContactsDTO> createContacts(@RequestBody ContactsDTO contactsDTO) throws URISyntaxException {
        log.debug("REST request to save Contacts : {}", contactsDTO);
        if (contactsDTO.getId() != null) {
            throw new BadRequestAlertException("A new contacts cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactsDTO result = contactsService.save(contactsDTO);
        return ResponseEntity.created(new URI("/api/contacts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contacts : Updates an existing contacts.
     *
     * @param contactsDTO the contactsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contactsDTO,
     * or with status 400 (Bad Request) if the contactsDTO is not valid,
     * or with status 500 (Internal Server Error) if the contactsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contacts")
    public ResponseEntity<ContactsDTO> updateContacts(@RequestBody ContactsDTO contactsDTO) throws URISyntaxException {
        log.debug("REST request to update Contacts : {}", contactsDTO);
        if (contactsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactsDTO result = contactsService.save(contactsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contactsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contacts : get all the contacts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contacts in body
     */
    @GetMapping("/contacts")
    public List<ContactsDTO> getAllContacts() {
        log.debug("REST request to get all Contacts");
        return contactsService.findAll();
    }

    /**
     * GET  /contacts/:id : get the "id" contacts.
     *
     * @param id the id of the contactsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contactsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/contacts/{id}")
    public ResponseEntity<ContactsDTO> getContacts(@PathVariable Long id) {
        log.debug("REST request to get Contacts : {}", id);
        Optional<ContactsDTO> contactsDTO = contactsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactsDTO);
    }

    /**
     * DELETE  /contacts/:id : delete the "id" contacts.
     *
     * @param id the id of the contactsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contacts/{id}")
    public ResponseEntity<Void> deleteContacts(@PathVariable Long id) {
        log.debug("REST request to delete Contacts : {}", id);
        contactsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
