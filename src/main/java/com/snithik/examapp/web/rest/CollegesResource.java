package com.snithik.examapp.web.rest;
import com.snithik.examapp.service.CollegesService;
import com.snithik.examapp.web.rest.errors.BadRequestAlertException;
import com.snithik.examapp.web.rest.util.HeaderUtil;
import com.snithik.examapp.service.dto.CollegesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Colleges.
 */
@RestController
@RequestMapping("/api")
public class CollegesResource {

    private final Logger log = LoggerFactory.getLogger(CollegesResource.class);

    private static final String ENTITY_NAME = "colleges";

    private final CollegesService collegesService;

    public CollegesResource(CollegesService collegesService) {
        this.collegesService = collegesService;
    }

    /**
     * POST  /colleges : Create a new colleges.
     *
     * @param collegesDTO the collegesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new collegesDTO, or with status 400 (Bad Request) if the colleges has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/colleges")
    public ResponseEntity<CollegesDTO> createColleges(@RequestBody CollegesDTO collegesDTO) throws URISyntaxException {
        log.debug("REST request to save Colleges : {}", collegesDTO);
        if (collegesDTO.getId() != null) {
            throw new BadRequestAlertException("A new colleges cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CollegesDTO result = collegesService.save(collegesDTO);
        return ResponseEntity.created(new URI("/api/colleges/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /colleges : Updates an existing colleges.
     *
     * @param collegesDTO the collegesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated collegesDTO,
     * or with status 400 (Bad Request) if the collegesDTO is not valid,
     * or with status 500 (Internal Server Error) if the collegesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/colleges")
    public ResponseEntity<CollegesDTO> updateColleges(@RequestBody CollegesDTO collegesDTO) throws URISyntaxException {
        log.debug("REST request to update Colleges : {}", collegesDTO);
        if (collegesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CollegesDTO result = collegesService.save(collegesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, collegesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /colleges : get all the colleges.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of colleges in body
     */
    @GetMapping("/colleges")
    public List<CollegesDTO> getAllColleges() {
        log.debug("REST request to get all Colleges");
        return collegesService.findAll();
    }

    /**
     * GET  /colleges/:id : get the "id" colleges.
     *
     * @param id the id of the collegesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the collegesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/colleges/{id}")
    public ResponseEntity<CollegesDTO> getColleges(@PathVariable Long id) {
        log.debug("REST request to get Colleges : {}", id);
        Optional<CollegesDTO> collegesDTO = collegesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(collegesDTO);
    }

    /**
     * DELETE  /colleges/:id : delete the "id" colleges.
     *
     * @param id the id of the collegesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/colleges/{id}")
    public ResponseEntity<Void> deleteColleges(@PathVariable Long id) {
        log.debug("REST request to delete Colleges : {}", id);
        collegesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
