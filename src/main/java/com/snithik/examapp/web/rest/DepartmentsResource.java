package com.snithik.examapp.web.rest;
import com.snithik.examapp.service.DepartmentsService;
import com.snithik.examapp.web.rest.errors.BadRequestAlertException;
import com.snithik.examapp.web.rest.util.HeaderUtil;
import com.snithik.examapp.service.dto.DepartmentsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Departments.
 */
@RestController
@RequestMapping("/api")
public class DepartmentsResource {

    private final Logger log = LoggerFactory.getLogger(DepartmentsResource.class);

    private static final String ENTITY_NAME = "departments";

    private final DepartmentsService departmentsService;

    public DepartmentsResource(DepartmentsService departmentsService) {
        this.departmentsService = departmentsService;
    }

    /**
     * POST  /departments : Create a new departments.
     *
     * @param departmentsDTO the departmentsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new departmentsDTO, or with status 400 (Bad Request) if the departments has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/departments")
    public ResponseEntity<DepartmentsDTO> createDepartments(@RequestBody DepartmentsDTO departmentsDTO) throws URISyntaxException {
        log.debug("REST request to save Departments : {}", departmentsDTO);
        if (departmentsDTO.getId() != null) {
            throw new BadRequestAlertException("A new departments cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DepartmentsDTO result = departmentsService.save(departmentsDTO);
        return ResponseEntity.created(new URI("/api/departments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /departments : Updates an existing departments.
     *
     * @param departmentsDTO the departmentsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated departmentsDTO,
     * or with status 400 (Bad Request) if the departmentsDTO is not valid,
     * or with status 500 (Internal Server Error) if the departmentsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/departments")
    public ResponseEntity<DepartmentsDTO> updateDepartments(@RequestBody DepartmentsDTO departmentsDTO) throws URISyntaxException {
        log.debug("REST request to update Departments : {}", departmentsDTO);
        if (departmentsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DepartmentsDTO result = departmentsService.save(departmentsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, departmentsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /departments : get all the departments.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of departments in body
     */
    @GetMapping("/departments")
    public List<DepartmentsDTO> getAllDepartments() {
        log.debug("REST request to get all Departments");
        return departmentsService.findAll();
    }

    /**
     * GET  /departments/:id : get the "id" departments.
     *
     * @param id the id of the departmentsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the departmentsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/departments/{id}")
    public ResponseEntity<DepartmentsDTO> getDepartments(@PathVariable Long id) {
        log.debug("REST request to get Departments : {}", id);
        Optional<DepartmentsDTO> departmentsDTO = departmentsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(departmentsDTO);
    }

    /**
     * DELETE  /departments/:id : delete the "id" departments.
     *
     * @param id the id of the departmentsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/departments/{id}")
    public ResponseEntity<Void> deleteDepartments(@PathVariable Long id) {
        log.debug("REST request to delete Departments : {}", id);
        departmentsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
