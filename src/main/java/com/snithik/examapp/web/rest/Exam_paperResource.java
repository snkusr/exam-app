package com.snithik.examapp.web.rest;
import com.snithik.examapp.service.Exam_paperService;
import com.snithik.examapp.web.rest.errors.BadRequestAlertException;
import com.snithik.examapp.web.rest.util.HeaderUtil;
import com.snithik.examapp.service.dto.Exam_paperDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Exam_paper.
 */
@RestController
@RequestMapping("/api")
public class Exam_paperResource {

    private final Logger log = LoggerFactory.getLogger(Exam_paperResource.class);

    private static final String ENTITY_NAME = "exam_paper";

    private final Exam_paperService exam_paperService;

    public Exam_paperResource(Exam_paperService exam_paperService) {
        this.exam_paperService = exam_paperService;
    }

    /**
     * POST  /exam-papers : Create a new exam_paper.
     *
     * @param exam_paperDTO the exam_paperDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new exam_paperDTO, or with status 400 (Bad Request) if the exam_paper has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/exam-papers")
    public ResponseEntity<Exam_paperDTO> createExam_paper(@RequestBody Exam_paperDTO exam_paperDTO) throws URISyntaxException {
        log.debug("REST request to save Exam_paper : {}", exam_paperDTO);
        if (exam_paperDTO.getId() != null) {
            throw new BadRequestAlertException("A new exam_paper cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Exam_paperDTO result = exam_paperService.save(exam_paperDTO);
        return ResponseEntity.created(new URI("/api/exam-papers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /exam-papers : Updates an existing exam_paper.
     *
     * @param exam_paperDTO the exam_paperDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated exam_paperDTO,
     * or with status 400 (Bad Request) if the exam_paperDTO is not valid,
     * or with status 500 (Internal Server Error) if the exam_paperDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/exam-papers")
    public ResponseEntity<Exam_paperDTO> updateExam_paper(@RequestBody Exam_paperDTO exam_paperDTO) throws URISyntaxException {
        log.debug("REST request to update Exam_paper : {}", exam_paperDTO);
        if (exam_paperDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Exam_paperDTO result = exam_paperService.save(exam_paperDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, exam_paperDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /exam-papers : get all the exam_papers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of exam_papers in body
     */
    @GetMapping("/exam-papers")
    public List<Exam_paperDTO> getAllExam_papers() {
        log.debug("REST request to get all Exam_papers");
        return exam_paperService.findAll();
    }

    /**
     * GET  /exam-papers/:id : get the "id" exam_paper.
     *
     * @param id the id of the exam_paperDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the exam_paperDTO, or with status 404 (Not Found)
     */
    @GetMapping("/exam-papers/{id}")
    public ResponseEntity<Exam_paperDTO> getExam_paper(@PathVariable Long id) {
        log.debug("REST request to get Exam_paper : {}", id);
        Optional<Exam_paperDTO> exam_paperDTO = exam_paperService.findOne(id);
        return ResponseUtil.wrapOrNotFound(exam_paperDTO);
    }

    /**
     * DELETE  /exam-papers/:id : delete the "id" exam_paper.
     *
     * @param id the id of the exam_paperDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/exam-papers/{id}")
    public ResponseEntity<Void> deleteExam_paper(@PathVariable Long id) {
        log.debug("REST request to delete Exam_paper : {}", id);
        exam_paperService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
