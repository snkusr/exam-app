package com.snithik.examapp.web.rest;
import com.snithik.examapp.service.StudentsService;
import com.snithik.examapp.web.rest.errors.BadRequestAlertException;
import com.snithik.examapp.web.rest.util.HeaderUtil;
import com.snithik.examapp.service.dto.StudentsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Students.
 */
@RestController
@RequestMapping("/api")
public class StudentsResource {

    private final Logger log = LoggerFactory.getLogger(StudentsResource.class);

    private static final String ENTITY_NAME = "students";

    private final StudentsService studentsService;

    public StudentsResource(StudentsService studentsService) {
        this.studentsService = studentsService;
    }

    /**
     * POST  /students : Create a new students.
     *
     * @param studentsDTO the studentsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new studentsDTO, or with status 400 (Bad Request) if the students has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/students")
    public ResponseEntity<StudentsDTO> createStudents(@RequestBody StudentsDTO studentsDTO) throws URISyntaxException {
        log.debug("REST request to save Students : {}", studentsDTO);
        if (studentsDTO.getId() != null) {
            throw new BadRequestAlertException("A new students cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StudentsDTO result = studentsService.save(studentsDTO);
        return ResponseEntity.created(new URI("/api/students/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /students : Updates an existing students.
     *
     * @param studentsDTO the studentsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated studentsDTO,
     * or with status 400 (Bad Request) if the studentsDTO is not valid,
     * or with status 500 (Internal Server Error) if the studentsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/students")
    public ResponseEntity<StudentsDTO> updateStudents(@RequestBody StudentsDTO studentsDTO) throws URISyntaxException {
        log.debug("REST request to update Students : {}", studentsDTO);
        if (studentsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StudentsDTO result = studentsService.save(studentsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, studentsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /students : get all the students.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of students in body
     */
    @GetMapping("/students")
    public List<StudentsDTO> getAllStudents() {
        log.debug("REST request to get all Students");
        return studentsService.findAll();
    }

    /**
     * GET  /students/:id : get the "id" students.
     *
     * @param id the id of the studentsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the studentsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/students/{id}")
    public ResponseEntity<StudentsDTO> getStudents(@PathVariable Long id) {
        log.debug("REST request to get Students : {}", id);
        Optional<StudentsDTO> studentsDTO = studentsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(studentsDTO);
    }

    /**
     * DELETE  /students/:id : delete the "id" students.
     *
     * @param id the id of the studentsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/students/{id}")
    public ResponseEntity<Void> deleteStudents(@PathVariable Long id) {
        log.debug("REST request to delete Students : {}", id);
        studentsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
