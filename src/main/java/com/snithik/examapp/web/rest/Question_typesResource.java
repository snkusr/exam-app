package com.snithik.examapp.web.rest;
import com.snithik.examapp.service.Question_typesService;
import com.snithik.examapp.web.rest.errors.BadRequestAlertException;
import com.snithik.examapp.web.rest.util.HeaderUtil;
import com.snithik.examapp.service.dto.Question_typesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Question_types.
 */
@RestController
@RequestMapping("/api")
public class Question_typesResource {

    private final Logger log = LoggerFactory.getLogger(Question_typesResource.class);

    private static final String ENTITY_NAME = "question_types";

    private final Question_typesService question_typesService;

    public Question_typesResource(Question_typesService question_typesService) {
        this.question_typesService = question_typesService;
    }

    /**
     * POST  /question-types : Create a new question_types.
     *
     * @param question_typesDTO the question_typesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new question_typesDTO, or with status 400 (Bad Request) if the question_types has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/question-types")
    public ResponseEntity<Question_typesDTO> createQuestion_types(@RequestBody Question_typesDTO question_typesDTO) throws URISyntaxException {
        log.debug("REST request to save Question_types : {}", question_typesDTO);
        if (question_typesDTO.getId() != null) {
            throw new BadRequestAlertException("A new question_types cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Question_typesDTO result = question_typesService.save(question_typesDTO);
        return ResponseEntity.created(new URI("/api/question-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /question-types : Updates an existing question_types.
     *
     * @param question_typesDTO the question_typesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated question_typesDTO,
     * or with status 400 (Bad Request) if the question_typesDTO is not valid,
     * or with status 500 (Internal Server Error) if the question_typesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/question-types")
    public ResponseEntity<Question_typesDTO> updateQuestion_types(@RequestBody Question_typesDTO question_typesDTO) throws URISyntaxException {
        log.debug("REST request to update Question_types : {}", question_typesDTO);
        if (question_typesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Question_typesDTO result = question_typesService.save(question_typesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, question_typesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /question-types : get all the question_types.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of question_types in body
     */
    @GetMapping("/question-types")
    public List<Question_typesDTO> getAllQuestion_types() {
        log.debug("REST request to get all Question_types");
        return question_typesService.findAll();
    }

    /**
     * GET  /question-types/:id : get the "id" question_types.
     *
     * @param id the id of the question_typesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the question_typesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/question-types/{id}")
    public ResponseEntity<Question_typesDTO> getQuestion_types(@PathVariable Long id) {
        log.debug("REST request to get Question_types : {}", id);
        Optional<Question_typesDTO> question_typesDTO = question_typesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(question_typesDTO);
    }

    /**
     * DELETE  /question-types/:id : delete the "id" question_types.
     *
     * @param id the id of the question_typesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/question-types/{id}")
    public ResponseEntity<Void> deleteQuestion_types(@PathVariable Long id) {
        log.debug("REST request to delete Question_types : {}", id);
        question_typesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
