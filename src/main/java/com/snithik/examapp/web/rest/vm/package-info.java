/**
 * View Models used by Spring MVC REST controllers.
 */
package com.snithik.examapp.web.rest.vm;
