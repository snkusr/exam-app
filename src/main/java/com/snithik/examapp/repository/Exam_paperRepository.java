package com.snithik.examapp.repository;

import com.snithik.examapp.domain.Exam_paper;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Exam_paper entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Exam_paperRepository extends JpaRepository<Exam_paper, Long> {

}
