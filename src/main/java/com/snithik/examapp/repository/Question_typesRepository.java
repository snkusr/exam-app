package com.snithik.examapp.repository;

import com.snithik.examapp.domain.Question_types;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Question_types entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Question_typesRepository extends JpaRepository<Question_types, Long> {

}
