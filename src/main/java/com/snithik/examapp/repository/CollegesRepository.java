package com.snithik.examapp.repository;

import com.snithik.examapp.domain.Colleges;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Colleges entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CollegesRepository extends JpaRepository<Colleges, Long> {

}
