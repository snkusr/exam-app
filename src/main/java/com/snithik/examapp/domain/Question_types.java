package com.snithik.examapp.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Question_types.
 */
@Entity
@Table(name = "question_types")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Question_types implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type_code")
    private String type_code;

    @Column(name = "type_name")
    private String type_name;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "creation_date")
    private LocalDate creation_date;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "last_update_date")
    private LocalDate last_update_date;

    @Column(name = "last_updated_by")
    private String last_updated_by;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType_code() {
        return type_code;
    }

    public Question_types type_code(String type_code) {
        this.type_code = type_code;
        return this;
    }

    public void setType_code(String type_code) {
        this.type_code = type_code;
    }

    public String getType_name() {
        return type_name;
    }

    public Question_types type_name(String type_name) {
        this.type_name = type_name;
        return this;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public Boolean isStatus() {
        return status;
    }

    public Question_types status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public Question_types creation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
        return this;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Question_types created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public Question_types last_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
        return this;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public Question_types last_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
        return this;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Question_types question_types = (Question_types) o;
        if (question_types.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), question_types.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Question_types{" +
            "id=" + getId() +
            ", type_code='" + getType_code() + "'" +
            ", type_name='" + getType_name() + "'" +
            ", status='" + isStatus() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            "}";
    }
}
