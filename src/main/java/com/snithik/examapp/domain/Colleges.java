package com.snithik.examapp.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Colleges.
 */
@Entity
@Table(name = "colleges")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Colleges implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "college_name")
    private String college_name;

    @Column(name = "college_code")
    private String college_code;

    @Column(name = "affiliated_university")
    private String affiliated_university;

    @Column(name = "address")
    private String address;

    @Column(name = "address_2")
    private String address2;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "zip_code")
    private String zip_code;

    @Column(name = "contact_number")
    private Long contact_number;

    @Column(name = "email")
    private String email;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "creation_date")
    private LocalDate creation_date;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "last_update_date")
    private LocalDate last_update_date;

    @Column(name = "last_update_by")
    private String last_update_by;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCollege_name() {
        return college_name;
    }

    public Colleges college_name(String college_name) {
        this.college_name = college_name;
        return this;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    public String getCollege_code() {
        return college_code;
    }

    public Colleges college_code(String college_code) {
        this.college_code = college_code;
        return this;
    }

    public void setCollege_code(String college_code) {
        this.college_code = college_code;
    }

    public String getAffiliated_university() {
        return affiliated_university;
    }

    public Colleges affiliated_university(String affiliated_university) {
        this.affiliated_university = affiliated_university;
        return this;
    }

    public void setAffiliated_university(String affiliated_university) {
        this.affiliated_university = affiliated_university;
    }

    public String getAddress() {
        return address;
    }

    public Colleges address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public Colleges address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public Colleges city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public Colleges state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public Colleges country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip_code() {
        return zip_code;
    }

    public Colleges zip_code(String zip_code) {
        this.zip_code = zip_code;
        return this;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public Long getContact_number() {
        return contact_number;
    }

    public Colleges contact_number(Long contact_number) {
        this.contact_number = contact_number;
        return this;
    }

    public void setContact_number(Long contact_number) {
        this.contact_number = contact_number;
    }

    public String getEmail() {
        return email;
    }

    public Colleges email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isStatus() {
        return status;
    }

    public Colleges status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public Colleges creation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
        return this;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Colleges created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public Colleges last_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
        return this;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_update_by() {
        return last_update_by;
    }

    public Colleges last_update_by(String last_update_by) {
        this.last_update_by = last_update_by;
        return this;
    }

    public void setLast_update_by(String last_update_by) {
        this.last_update_by = last_update_by;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Colleges colleges = (Colleges) o;
        if (colleges.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), colleges.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Colleges{" +
            "id=" + getId() +
            ", college_name='" + getCollege_name() + "'" +
            ", college_code='" + getCollege_code() + "'" +
            ", affiliated_university='" + getAffiliated_university() + "'" +
            ", address='" + getAddress() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", zip_code='" + getZip_code() + "'" +
            ", contact_number=" + getContact_number() +
            ", email='" + getEmail() + "'" +
            ", status='" + isStatus() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_update_by='" + getLast_update_by() + "'" +
            "}";
    }
}
