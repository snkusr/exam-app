package com.snithik.examapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Answers.
 */
@Entity
@Table(name = "answers")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Answers implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "answer")
    private String answer;

    @Column(name = "answered_date")
    private LocalDate answered_date;

    @Column(name = "end_time")
    private LocalDate end_time;

    @Column(name = "start_time")
    private LocalDate start_time;

    @Column(name = "creation_date")
    private LocalDate creation_date;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "last_update_date")
    private LocalDate last_update_date;

    @Column(name = "last_updated_by")
    private LocalDate last_updated_by;

    @ManyToOne
    private Questions question_id;

    @ManyToOne
    private Students studentid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public Answers answer(String answer) {
        this.answer = answer;
        return this;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public LocalDate getAnswered_date() {
        return answered_date;
    }

    public Answers answered_date(LocalDate answered_date) {
        this.answered_date = answered_date;
        return this;
    }

    public void setAnswered_date(LocalDate answered_date) {
        this.answered_date = answered_date;
    }

    public LocalDate getEnd_time() {
        return end_time;
    }

    public Answers end_time(LocalDate end_time) {
        this.end_time = end_time;
        return this;
    }

    public void setEnd_time(LocalDate end_time) {
        this.end_time = end_time;
    }

    public LocalDate getStart_time() {
        return start_time;
    }

    public Answers start_time(LocalDate start_time) {
        this.start_time = start_time;
        return this;
    }

    public void setStart_time(LocalDate start_time) {
        this.start_time = start_time;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public Answers creation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
        return this;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Answers created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public Answers last_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
        return this;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public LocalDate getLast_updated_by() {
        return last_updated_by;
    }

    public Answers last_updated_by(LocalDate last_updated_by) {
        this.last_updated_by = last_updated_by;
        return this;
    }

    public void setLast_updated_by(LocalDate last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public Questions getQuestion_id() {
        return question_id;
    }

    public Answers question_id(Questions questions) {
        this.question_id = questions;
        return this;
    }

    public void setQuestion_id(Questions questions) {
        this.question_id = questions;
    }

    public Students getStudentid() {
        return studentid;
    }

    public Answers studentid(Students students) {
        this.studentid = students;
        return this;
    }

    public void setStudentid(Students students) {
        this.studentid = students;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Answers answers = (Answers) o;
        if (answers.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), answers.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Answers{" +
            "id=" + getId() +
            ", answer='" + getAnswer() + "'" +
            ", answered_date='" + getAnswered_date() + "'" +
            ", end_time='" + getEnd_time() + "'" +
            ", start_time='" + getStart_time() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            "}";
    }
}
