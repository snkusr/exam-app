package com.snithik.examapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Students.
 */
@Entity
@Table(name = "students")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Students implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "student_name")
    private String student_name;

    @Column(name = "mobile")
    private Long mobile;

    @Column(name = "email")
    private String email;

    @Column(name = "studying_year")
    private String studying_year;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "creation_date")
    private LocalDate creation_date;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "last_update_date")
    private LocalDate last_update_date;

    @Column(name = "last_updated_by")
    private String last_updated_by;

    @Column(name = "department")
    private String department;

    @Column(name = "emcetecet_rank")
    private Long emcetecet_rank;

    @Column(name = "current_cgpa")
    private Float current_CGPA;

    @Column(name = "fathers_occupation")
    private String fathers_occupation;

    @Column(name = "mothers_occupation")
    private String mothers_occupation;

    @Column(name = "siblings")
    private Integer siblings;

    @Column(name = "family_income")
    private String family_income;

    @Column(name = "native_place")
    private String native_place;

    @Column(name = "languages_known")
    private String languages_known;

    @ManyToOne
    @JsonIgnoreProperties("students")
    private Colleges collegeid;

    @ManyToOne
    @JsonIgnoreProperties("students")
    private Departments departmentid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public Students student_name(String student_name) {
        this.student_name = student_name;
        return this;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public Long getMobile() {
        return mobile;
    }

    public Students mobile(Long mobile) {
        this.mobile = mobile;
        return this;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public Students email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStudying_year() {
        return studying_year;
    }

    public Students studying_year(String studying_year) {
        this.studying_year = studying_year;
        return this;
    }

    public void setStudying_year(String studying_year) {
        this.studying_year = studying_year;
    }

    public Boolean isStatus() {
        return status;
    }

    public Students status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public Students creation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
        return this;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Students created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public Students last_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
        return this;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public Students last_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
        return this;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public String getDepartment() {
        return department;
    }

    public Students department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Long getEmcetecet_rank() {
        return emcetecet_rank;
    }

    public Students emcetecet_rank(Long emcetecet_rank) {
        this.emcetecet_rank = emcetecet_rank;
        return this;
    }

    public void setEmcetecet_rank(Long emcetecet_rank) {
        this.emcetecet_rank = emcetecet_rank;
    }

    public Float getCurrent_CGPA() {
        return current_CGPA;
    }

    public Students current_CGPA(Float current_CGPA) {
        this.current_CGPA = current_CGPA;
        return this;
    }

    public void setCurrent_CGPA(Float current_CGPA) {
        this.current_CGPA = current_CGPA;
    }

    public String getFathers_occupation() {
        return fathers_occupation;
    }

    public Students fathers_occupation(String fathers_occupation) {
        this.fathers_occupation = fathers_occupation;
        return this;
    }

    public void setFathers_occupation(String fathers_occupation) {
        this.fathers_occupation = fathers_occupation;
    }

    public String getMothers_occupation() {
        return mothers_occupation;
    }

    public Students mothers_occupation(String mothers_occupation) {
        this.mothers_occupation = mothers_occupation;
        return this;
    }

    public void setMothers_occupation(String mothers_occupation) {
        this.mothers_occupation = mothers_occupation;
    }

    public Integer getSiblings() {
        return siblings;
    }

    public Students siblings(Integer siblings) {
        this.siblings = siblings;
        return this;
    }

    public void setSiblings(Integer siblings) {
        this.siblings = siblings;
    }

    public String getFamily_income() {
        return family_income;
    }

    public Students family_income(String family_income) {
        this.family_income = family_income;
        return this;
    }

    public void setFamily_income(String family_income) {
        this.family_income = family_income;
    }

    public String getNative_place() {
        return native_place;
    }

    public Students native_place(String native_place) {
        this.native_place = native_place;
        return this;
    }

    public void setNative_place(String native_place) {
        this.native_place = native_place;
    }

    public String getLanguages_known() {
        return languages_known;
    }

    public Students languages_known(String languages_known) {
        this.languages_known = languages_known;
        return this;
    }

    public void setLanguages_known(String languages_known) {
        this.languages_known = languages_known;
    }

    public Colleges getCollegeid() {
        return collegeid;
    }

    public Students collegeid(Colleges colleges) {
        this.collegeid = colleges;
        return this;
    }

    public void setCollegeid(Colleges colleges) {
        this.collegeid = colleges;
    }

    public Departments getDepartmentid() {
        return departmentid;
    }

    public Students departmentid(Departments departments) {
        this.departmentid = departments;
        return this;
    }

    public void setDepartmentid(Departments departments) {
        this.departmentid = departments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Students students = (Students) o;
        if (students.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), students.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Students{" +
            "id=" + getId() +
            ", student_name='" + getStudent_name() + "'" +
            ", mobile=" + getMobile() +
            ", email='" + getEmail() + "'" +
            ", studying_year='" + getStudying_year() + "'" +
            ", status='" + isStatus() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            ", department='" + getDepartment() + "'" +
            ", emcetecet_rank=" + getEmcetecet_rank() +
            ", current_CGPA=" + getCurrent_CGPA() +
            ", fathers_occupation='" + getFathers_occupation() + "'" +
            ", mothers_occupation='" + getMothers_occupation() + "'" +
            ", siblings=" + getSiblings() +
            ", family_income='" + getFamily_income() + "'" +
            ", native_place='" + getNative_place() + "'" +
            ", languages_known='" + getLanguages_known() + "'" +
            "}";
    }
}
