package com.snithik.examapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Exam_paper.
 */
@Entity
@Table(name = "exam_paper")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Exam_paper implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "creation_date")
    private LocalDate creation_date;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "last_update_date")
    private LocalDate last_update_date;

    @Column(name = "last_updated_by")
    private String last_updated_by;

    @ManyToOne
    private Colleges collegeid;

    @ManyToOne
    private Questions questionid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Exam_paper name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public Exam_paper creation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
        return this;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Exam_paper created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public Exam_paper last_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
        return this;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public Exam_paper last_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
        return this;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public Colleges getCollegeid() {
        return collegeid;
    }

    public Exam_paper collegeid(Colleges colleges) {
        this.collegeid = colleges;
        return this;
    }

    public void setCollegeid(Colleges colleges) {
        this.collegeid = colleges;
    }

    public Questions getQuestionid() {
        return questionid;
    }

    public Exam_paper questionid(Questions questions) {
        this.questionid = questions;
        return this;
    }

    public void setQuestionid(Questions questions) {
        this.questionid = questions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Exam_paper exam_paper = (Exam_paper) o;
        if (exam_paper.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), exam_paper.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Exam_paper{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            "}";
    }
}
