package com.snithik.examapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Contacts.
 */
@Entity
@Table(name = "contacts")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Contacts implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contact_name")
    private String contact_name;

    @Column(name = "mobile")
    private Long mobile;

    @Column(name = "email")
    private String email;

    @Column(name = "designation")
    private String designation;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "creation_date")
    private LocalDate creation_date;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "last_update_date")
    private LocalDate last_update_date;

    @Column(name = "last_updated_by")
    private String last_updated_by;

    @ManyToOne
    private Colleges collegeid;

    @ManyToOne
    private Departments departmentid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContact_name() {
        return contact_name;
    }

    public Contacts contact_name(String contact_name) {
        this.contact_name = contact_name;
        return this;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public Long getMobile() {
        return mobile;
    }

    public Contacts mobile(Long mobile) {
        this.mobile = mobile;
        return this;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public Contacts email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public Contacts designation(String designation) {
        this.designation = designation;
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Boolean isStatus() {
        return status;
    }

    public Contacts status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public Contacts creation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
        return this;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Contacts created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getLast_update_date() {
        return last_update_date;
    }

    public Contacts last_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
        return this;
    }

    public void setLast_update_date(LocalDate last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public Contacts last_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
        return this;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public Colleges getCollegeid() {
        return collegeid;
    }

    public Contacts collegeid(Colleges colleges) {
        this.collegeid = colleges;
        return this;
    }

    public void setCollegeid(Colleges colleges) {
        this.collegeid = colleges;
    }

    public Departments getDepartmentid() {
        return departmentid;
    }

    public Contacts departmentid(Departments departments) {
        this.departmentid = departments;
        return this;
    }

    public void setDepartmentid(Departments departments) {
        this.departmentid = departments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contacts contacts = (Contacts) o;
        if (contacts.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contacts.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Contacts{" +
            "id=" + getId() +
            ", contact_name='" + getContact_name() + "'" +
            ", mobile=" + getMobile() +
            ", email='" + getEmail() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", status='" + isStatus() + "'" +
            ", creation_date='" + getCreation_date() + "'" +
            ", created_by='" + getCreated_by() + "'" +
            ", last_update_date='" + getLast_update_date() + "'" +
            ", last_updated_by='" + getLast_updated_by() + "'" +
            "}";
    }
}
