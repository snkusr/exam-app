import { Moment } from 'moment';

export interface IQuestions {
    id?: number;
    question_name?: string;
    answer?: string;
    option1?: string;
    option2?: string;
    option3?: string;
    option4?: string;
    status?: boolean;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_updated_by?: string;
    questiontypeidId?: number;
}

export class Questions implements IQuestions {
    constructor(
        public id?: number,
        public question_name?: string,
        public answer?: string,
        public option1?: string,
        public option2?: string,
        public option3?: string,
        public option4?: string,
        public status?: boolean,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_updated_by?: string,
        public questiontypeidId?: number
    ) {
        this.status = this.status || false;
    }
}
