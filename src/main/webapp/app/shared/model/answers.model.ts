import { Moment } from 'moment';

export interface IAnswers {
    id?: number;
    answer?: string;
    answered_date?: Moment;
    end_time?: Moment;
    start_time?: Moment;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_updated_by?: Moment;
    question_idId?: number;
    studentidId?: number;
}

export class Answers implements IAnswers {
    constructor(
        public id?: number,
        public answer?: string,
        public answered_date?: Moment,
        public end_time?: Moment,
        public start_time?: Moment,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_updated_by?: Moment,
        public question_idId?: number,
        public studentidId?: number
    ) {}
}
