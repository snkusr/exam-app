import { Moment } from 'moment';

export interface IContacts {
    id?: number;
    contact_name?: string;
    mobile?: number;
    email?: string;
    designation?: string;
    status?: boolean;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_updated_by?: string;
    collegeidId?: number;
    departmentidId?: number;
}

export class Contacts implements IContacts {
    constructor(
        public id?: number,
        public contact_name?: string,
        public mobile?: number,
        public email?: string,
        public designation?: string,
        public status?: boolean,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_updated_by?: string,
        public collegeidId?: number,
        public departmentidId?: number
    ) {
        this.status = this.status || false;
    }
}
