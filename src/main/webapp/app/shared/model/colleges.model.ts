import { Moment } from 'moment';

export interface IColleges {
    id?: number;
    college_name?: string;
    college_code?: string;
    affiliated_university?: string;
    address?: string;
    address2?: string;
    city?: string;
    state?: string;
    country?: string;
    zip_code?: string;
    contact_number?: number;
    email?: string;
    status?: boolean;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_update_by?: string;
}

export class Colleges implements IColleges {
    constructor(
        public id?: number,
        public college_name?: string,
        public college_code?: string,
        public affiliated_university?: string,
        public address?: string,
        public address2?: string,
        public city?: string,
        public state?: string,
        public country?: string,
        public zip_code?: string,
        public contact_number?: number,
        public email?: string,
        public status?: boolean,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_update_by?: string
    ) {
        this.status = this.status || false;
    }
}
