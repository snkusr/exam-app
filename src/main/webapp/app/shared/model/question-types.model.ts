import { Moment } from 'moment';

export interface IQuestion_types {
    id?: number;
    type_code?: string;
    type_name?: string;
    status?: boolean;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_updated_by?: string;
}

export class Question_types implements IQuestion_types {
    constructor(
        public id?: number,
        public type_code?: string,
        public type_name?: string,
        public status?: boolean,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_updated_by?: string
    ) {
        this.status = this.status || false;
    }
}
