import { Moment } from 'moment';

export interface IStudents {
    id?: number;
    student_name?: string;
    mobile?: number;
    email?: string;
    studying_year?: string;
    status?: boolean;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_updated_by?: string;
    department?: string;
    emcetecet_rank?: number;
    current_CGPA?: number;
    fathers_occupation?: string;
    mothers_occupation?: string;
    siblings?: number;
    family_income?: string;
    native_place?: string;
    languages_known?: string;
    collegeidId?: number;
    departmentidId?: number;
}

export class Students implements IStudents {
    constructor(
        public id?: number,
        public student_name?: string,
        public mobile?: number,
        public email?: string,
        public studying_year?: string,
        public status?: boolean,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_updated_by?: string,
        public department?: string,
        public emcetecet_rank?: number,
        public current_CGPA?: number,
        public fathers_occupation?: string,
        public mothers_occupation?: string,
        public siblings?: number,
        public family_income?: string,
        public native_place?: string,
        public languages_known?: string,
        public collegeidId?: number,
        public departmentidId?: number
    ) {
        this.status = this.status || false;
    }
}
