import { Moment } from 'moment';

export interface IDepartments {
    id?: number;
    department_name?: string;
    department_code?: string;
    status?: boolean;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_updated_by?: string;
}

export class Departments implements IDepartments {
    constructor(
        public id?: number,
        public department_name?: string,
        public department_code?: string,
        public status?: boolean,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_updated_by?: string
    ) {
        this.status = this.status || false;
    }
}
