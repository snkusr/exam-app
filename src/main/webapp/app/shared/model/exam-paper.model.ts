import { Moment } from 'moment';

export interface IExam_paper {
    id?: number;
    name?: string;
    creation_date?: Moment;
    created_by?: string;
    last_update_date?: Moment;
    last_updated_by?: string;
    collegeidId?: number;
    questionidId?: number;
}

export class Exam_paper implements IExam_paper {
    constructor(
        public id?: number,
        public name?: string,
        public creation_date?: Moment,
        public created_by?: string,
        public last_update_date?: Moment,
        public last_updated_by?: string,
        public collegeidId?: number,
        public questionidId?: number
    ) {}
}
