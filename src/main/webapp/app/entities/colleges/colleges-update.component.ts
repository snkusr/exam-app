import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { IColleges } from 'app/shared/model/colleges.model';
import { CollegesService } from './colleges.service';

@Component({
    selector: 'jhi-colleges-update',
    templateUrl: './colleges-update.component.html'
})
export class CollegesUpdateComponent implements OnInit {
    colleges: IColleges;
    isSaving: boolean;
    creation_dateDp: any;
    last_update_dateDp: any;

    constructor(protected collegesService: CollegesService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ colleges }) => {
            this.colleges = colleges;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.colleges.id !== undefined) {
            this.subscribeToSaveResponse(this.collegesService.update(this.colleges));
        } else {
            this.subscribeToSaveResponse(this.collegesService.create(this.colleges));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IColleges>>) {
        result.subscribe((res: HttpResponse<IColleges>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
