import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IColleges } from 'app/shared/model/colleges.model';

type EntityResponseType = HttpResponse<IColleges>;
type EntityArrayResponseType = HttpResponse<IColleges[]>;

@Injectable({ providedIn: 'root' })
export class CollegesService {
    public resourceUrl = SERVER_API_URL + 'api/colleges';

    constructor(protected http: HttpClient) {}

    create(colleges: IColleges): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(colleges);
        return this.http
            .post<IColleges>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(colleges: IColleges): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(colleges);
        return this.http
            .put<IColleges>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IColleges>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IColleges[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(colleges: IColleges): IColleges {
        const copy: IColleges = Object.assign({}, colleges, {
            creation_date:
                colleges.creation_date != null && colleges.creation_date.isValid() ? colleges.creation_date.format(DATE_FORMAT) : null,
            last_update_date:
                colleges.last_update_date != null && colleges.last_update_date.isValid()
                    ? colleges.last_update_date.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((colleges: IColleges) => {
                colleges.creation_date = colleges.creation_date != null ? moment(colleges.creation_date) : null;
                colleges.last_update_date = colleges.last_update_date != null ? moment(colleges.last_update_date) : null;
            });
        }
        return res;
    }
}
