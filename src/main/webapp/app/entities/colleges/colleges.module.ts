import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ExamappSharedModule } from 'app/shared';
import {
    CollegesComponent,
    CollegesDetailComponent,
    CollegesUpdateComponent,
    CollegesDeletePopupComponent,
    CollegesDeleteDialogComponent,
    collegesRoute,
    collegesPopupRoute
} from './';

const ENTITY_STATES = [...collegesRoute, ...collegesPopupRoute];

@NgModule({
    imports: [ExamappSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CollegesComponent,
        CollegesDetailComponent,
        CollegesUpdateComponent,
        CollegesDeleteDialogComponent,
        CollegesDeletePopupComponent
    ],
    entryComponents: [CollegesComponent, CollegesUpdateComponent, CollegesDeleteDialogComponent, CollegesDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappCollegesModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
