import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IColleges } from 'app/shared/model/colleges.model';

@Component({
    selector: 'jhi-colleges-detail',
    templateUrl: './colleges-detail.component.html'
})
export class CollegesDetailComponent implements OnInit {
    colleges: IColleges;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ colleges }) => {
            this.colleges = colleges;
        });
    }

    previousState() {
        window.history.back();
    }
}
