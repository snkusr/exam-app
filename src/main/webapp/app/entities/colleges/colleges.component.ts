import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IColleges } from 'app/shared/model/colleges.model';
import { AccountService } from 'app/core';
import { CollegesService } from './colleges.service';

@Component({
    selector: 'jhi-colleges',
    templateUrl: './colleges.component.html'
})
export class CollegesComponent implements OnInit, OnDestroy {
    colleges: IColleges[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected collegesService: CollegesService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.collegesService
            .query()
            .pipe(
                filter((res: HttpResponse<IColleges[]>) => res.ok),
                map((res: HttpResponse<IColleges[]>) => res.body)
            )
            .subscribe(
                (res: IColleges[]) => {
                    this.colleges = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInColleges();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IColleges) {
        return item.id;
    }

    registerChangeInColleges() {
        this.eventSubscriber = this.eventManager.subscribe('collegesListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
