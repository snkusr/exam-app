import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Colleges } from 'app/shared/model/colleges.model';
import { CollegesService } from './colleges.service';
import { CollegesComponent } from './colleges.component';
import { CollegesDetailComponent } from './colleges-detail.component';
import { CollegesUpdateComponent } from './colleges-update.component';
import { CollegesDeletePopupComponent } from './colleges-delete-dialog.component';
import { IColleges } from 'app/shared/model/colleges.model';

@Injectable({ providedIn: 'root' })
export class CollegesResolve implements Resolve<IColleges> {
    constructor(private service: CollegesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IColleges> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Colleges>) => response.ok),
                map((colleges: HttpResponse<Colleges>) => colleges.body)
            );
        }
        return of(new Colleges());
    }
}

export const collegesRoute: Routes = [
    {
        path: '',
        component: CollegesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.colleges.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CollegesDetailComponent,
        resolve: {
            colleges: CollegesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.colleges.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CollegesUpdateComponent,
        resolve: {
            colleges: CollegesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.colleges.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CollegesUpdateComponent,
        resolve: {
            colleges: CollegesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.colleges.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const collegesPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CollegesDeletePopupComponent,
        resolve: {
            colleges: CollegesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.colleges.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
