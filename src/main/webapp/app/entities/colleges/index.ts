export * from './colleges.service';
export * from './colleges-update.component';
export * from './colleges-delete-dialog.component';
export * from './colleges-detail.component';
export * from './colleges.component';
export * from './colleges.route';
