import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IColleges } from 'app/shared/model/colleges.model';
import { CollegesService } from './colleges.service';

@Component({
    selector: 'jhi-colleges-delete-dialog',
    templateUrl: './colleges-delete-dialog.component.html'
})
export class CollegesDeleteDialogComponent {
    colleges: IColleges;

    constructor(protected collegesService: CollegesService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.collegesService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'collegesListModification',
                content: 'Deleted an colleges'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-colleges-delete-popup',
    template: ''
})
export class CollegesDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ colleges }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CollegesDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.colleges = colleges;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/colleges', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/colleges', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
