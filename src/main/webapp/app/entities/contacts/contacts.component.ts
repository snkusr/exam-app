import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IContacts } from 'app/shared/model/contacts.model';
import { AccountService } from 'app/core';
import { ContactsService } from './contacts.service';

@Component({
    selector: 'jhi-contacts',
    templateUrl: './contacts.component.html'
})
export class ContactsComponent implements OnInit, OnDestroy {
    contacts: IContacts[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected contactsService: ContactsService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.contactsService
            .query()
            .pipe(
                filter((res: HttpResponse<IContacts[]>) => res.ok),
                map((res: HttpResponse<IContacts[]>) => res.body)
            )
            .subscribe(
                (res: IContacts[]) => {
                    this.contacts = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInContacts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IContacts) {
        return item.id;
    }

    registerChangeInContacts() {
        this.eventSubscriber = this.eventManager.subscribe('contactsListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
