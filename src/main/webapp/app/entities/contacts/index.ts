export * from './contacts.service';
export * from './contacts-update.component';
export * from './contacts-delete-dialog.component';
export * from './contacts-detail.component';
export * from './contacts.component';
export * from './contacts.route';
