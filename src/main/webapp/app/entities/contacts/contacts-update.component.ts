import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IContacts } from 'app/shared/model/contacts.model';
import { ContactsService } from './contacts.service';
import { IColleges } from 'app/shared/model/colleges.model';
import { CollegesService } from 'app/entities/colleges';
import { IDepartments } from 'app/shared/model/departments.model';
import { DepartmentsService } from 'app/entities/departments';

@Component({
    selector: 'jhi-contacts-update',
    templateUrl: './contacts-update.component.html'
})
export class ContactsUpdateComponent implements OnInit {
    contacts: IContacts;
    isSaving: boolean;

    colleges: IColleges[];

    departments: IDepartments[];
    creation_dateDp: any;
    last_update_dateDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected contactsService: ContactsService,
        protected collegesService: CollegesService,
        protected departmentsService: DepartmentsService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ contacts }) => {
            this.contacts = contacts;
        });
        this.collegesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IColleges[]>) => mayBeOk.ok),
                map((response: HttpResponse<IColleges[]>) => response.body)
            )
            .subscribe((res: IColleges[]) => (this.colleges = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.departmentsService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IDepartments[]>) => mayBeOk.ok),
                map((response: HttpResponse<IDepartments[]>) => response.body)
            )
            .subscribe((res: IDepartments[]) => (this.departments = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.contacts.id !== undefined) {
            this.subscribeToSaveResponse(this.contactsService.update(this.contacts));
        } else {
            this.subscribeToSaveResponse(this.contactsService.create(this.contacts));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IContacts>>) {
        result.subscribe((res: HttpResponse<IContacts>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCollegesById(index: number, item: IColleges) {
        return item.id;
    }

    trackDepartmentsById(index: number, item: IDepartments) {
        return item.id;
    }
}
