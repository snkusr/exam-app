import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IContacts } from 'app/shared/model/contacts.model';
import { ContactsService } from './contacts.service';

@Component({
    selector: 'jhi-contacts-delete-dialog',
    templateUrl: './contacts-delete-dialog.component.html'
})
export class ContactsDeleteDialogComponent {
    contacts: IContacts;

    constructor(protected contactsService: ContactsService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.contactsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'contactsListModification',
                content: 'Deleted an contacts'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-contacts-delete-popup',
    template: ''
})
export class ContactsDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ contacts }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ContactsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.contacts = contacts;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/contacts', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/contacts', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
