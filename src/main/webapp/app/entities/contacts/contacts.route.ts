import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Contacts } from 'app/shared/model/contacts.model';
import { ContactsService } from './contacts.service';
import { ContactsComponent } from './contacts.component';
import { ContactsDetailComponent } from './contacts-detail.component';
import { ContactsUpdateComponent } from './contacts-update.component';
import { ContactsDeletePopupComponent } from './contacts-delete-dialog.component';
import { IContacts } from 'app/shared/model/contacts.model';

@Injectable({ providedIn: 'root' })
export class ContactsResolve implements Resolve<IContacts> {
    constructor(private service: ContactsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IContacts> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Contacts>) => response.ok),
                map((contacts: HttpResponse<Contacts>) => contacts.body)
            );
        }
        return of(new Contacts());
    }
}

export const contactsRoute: Routes = [
    {
        path: '',
        component: ContactsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.contacts.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ContactsDetailComponent,
        resolve: {
            contacts: ContactsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.contacts.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ContactsUpdateComponent,
        resolve: {
            contacts: ContactsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.contacts.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ContactsUpdateComponent,
        resolve: {
            contacts: ContactsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.contacts.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const contactsPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ContactsDeletePopupComponent,
        resolve: {
            contacts: ContactsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.contacts.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
