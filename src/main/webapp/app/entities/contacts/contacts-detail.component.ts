import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContacts } from 'app/shared/model/contacts.model';

@Component({
    selector: 'jhi-contacts-detail',
    templateUrl: './contacts-detail.component.html'
})
export class ContactsDetailComponent implements OnInit {
    contacts: IContacts;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ contacts }) => {
            this.contacts = contacts;
        });
    }

    previousState() {
        window.history.back();
    }
}
