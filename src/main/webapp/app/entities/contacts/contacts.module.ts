import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ExamappSharedModule } from 'app/shared';
import {
    ContactsComponent,
    ContactsDetailComponent,
    ContactsUpdateComponent,
    ContactsDeletePopupComponent,
    ContactsDeleteDialogComponent,
    contactsRoute,
    contactsPopupRoute
} from './';

const ENTITY_STATES = [...contactsRoute, ...contactsPopupRoute];

@NgModule({
    imports: [ExamappSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ContactsComponent,
        ContactsDetailComponent,
        ContactsUpdateComponent,
        ContactsDeleteDialogComponent,
        ContactsDeletePopupComponent
    ],
    entryComponents: [ContactsComponent, ContactsUpdateComponent, ContactsDeleteDialogComponent, ContactsDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappContactsModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
