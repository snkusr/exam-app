import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IContacts } from 'app/shared/model/contacts.model';

type EntityResponseType = HttpResponse<IContacts>;
type EntityArrayResponseType = HttpResponse<IContacts[]>;

@Injectable({ providedIn: 'root' })
export class ContactsService {
    public resourceUrl = SERVER_API_URL + 'api/contacts';

    constructor(protected http: HttpClient) {}

    create(contacts: IContacts): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(contacts);
        return this.http
            .post<IContacts>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(contacts: IContacts): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(contacts);
        return this.http
            .put<IContacts>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IContacts>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IContacts[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(contacts: IContacts): IContacts {
        const copy: IContacts = Object.assign({}, contacts, {
            creation_date:
                contacts.creation_date != null && contacts.creation_date.isValid() ? contacts.creation_date.format(DATE_FORMAT) : null,
            last_update_date:
                contacts.last_update_date != null && contacts.last_update_date.isValid()
                    ? contacts.last_update_date.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((contacts: IContacts) => {
                contacts.creation_date = contacts.creation_date != null ? moment(contacts.creation_date) : null;
                contacts.last_update_date = contacts.last_update_date != null ? moment(contacts.last_update_date) : null;
            });
        }
        return res;
    }
}
