import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IExam_paper } from 'app/shared/model/exam-paper.model';
import { Exam_paperService } from './exam-paper.service';
import { IColleges } from 'app/shared/model/colleges.model';
import { CollegesService } from 'app/entities/colleges';
import { IQuestions } from 'app/shared/model/questions.model';
import { QuestionsService } from 'app/entities/questions';

@Component({
    selector: 'jhi-exam-paper-update',
    templateUrl: './exam-paper-update.component.html'
})
export class Exam_paperUpdateComponent implements OnInit {
    exam_paper: IExam_paper;
    isSaving: boolean;

    colleges: IColleges[];

    questions: IQuestions[];
    creation_dateDp: any;
    last_update_dateDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected exam_paperService: Exam_paperService,
        protected collegesService: CollegesService,
        protected questionsService: QuestionsService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ exam_paper }) => {
            this.exam_paper = exam_paper;
        });
        this.collegesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IColleges[]>) => mayBeOk.ok),
                map((response: HttpResponse<IColleges[]>) => response.body)
            )
            .subscribe((res: IColleges[]) => (this.colleges = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.questionsService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IQuestions[]>) => mayBeOk.ok),
                map((response: HttpResponse<IQuestions[]>) => response.body)
            )
            .subscribe((res: IQuestions[]) => (this.questions = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.exam_paper.id !== undefined) {
            this.subscribeToSaveResponse(this.exam_paperService.update(this.exam_paper));
        } else {
            this.subscribeToSaveResponse(this.exam_paperService.create(this.exam_paper));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IExam_paper>>) {
        result.subscribe((res: HttpResponse<IExam_paper>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCollegesById(index: number, item: IColleges) {
        return item.id;
    }

    trackQuestionsById(index: number, item: IQuestions) {
        return item.id;
    }
}
