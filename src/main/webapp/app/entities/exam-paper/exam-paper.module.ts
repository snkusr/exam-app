import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ExamappSharedModule } from 'app/shared';
import {
    Exam_paperComponent,
    Exam_paperDetailComponent,
    Exam_paperUpdateComponent,
    Exam_paperDeletePopupComponent,
    Exam_paperDeleteDialogComponent,
    exam_paperRoute,
    exam_paperPopupRoute
} from './';

const ENTITY_STATES = [...exam_paperRoute, ...exam_paperPopupRoute];

@NgModule({
    imports: [ExamappSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        Exam_paperComponent,
        Exam_paperDetailComponent,
        Exam_paperUpdateComponent,
        Exam_paperDeleteDialogComponent,
        Exam_paperDeletePopupComponent
    ],
    entryComponents: [Exam_paperComponent, Exam_paperUpdateComponent, Exam_paperDeleteDialogComponent, Exam_paperDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappExam_paperModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
