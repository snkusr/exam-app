export * from './exam-paper.service';
export * from './exam-paper-update.component';
export * from './exam-paper-delete-dialog.component';
export * from './exam-paper-detail.component';
export * from './exam-paper.component';
export * from './exam-paper.route';
