import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Exam_paper } from 'app/shared/model/exam-paper.model';
import { Exam_paperService } from './exam-paper.service';
import { Exam_paperComponent } from './exam-paper.component';
import { Exam_paperDetailComponent } from './exam-paper-detail.component';
import { Exam_paperUpdateComponent } from './exam-paper-update.component';
import { Exam_paperDeletePopupComponent } from './exam-paper-delete-dialog.component';
import { IExam_paper } from 'app/shared/model/exam-paper.model';

@Injectable({ providedIn: 'root' })
export class Exam_paperResolve implements Resolve<IExam_paper> {
    constructor(private service: Exam_paperService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExam_paper> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Exam_paper>) => response.ok),
                map((exam_paper: HttpResponse<Exam_paper>) => exam_paper.body)
            );
        }
        return of(new Exam_paper());
    }
}

export const exam_paperRoute: Routes = [
    {
        path: '',
        component: Exam_paperComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.exam_paper.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: Exam_paperDetailComponent,
        resolve: {
            exam_paper: Exam_paperResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.exam_paper.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: Exam_paperUpdateComponent,
        resolve: {
            exam_paper: Exam_paperResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.exam_paper.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: Exam_paperUpdateComponent,
        resolve: {
            exam_paper: Exam_paperResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.exam_paper.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const exam_paperPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: Exam_paperDeletePopupComponent,
        resolve: {
            exam_paper: Exam_paperResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.exam_paper.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
