import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IExam_paper } from 'app/shared/model/exam-paper.model';
import { AccountService } from 'app/core';
import { Exam_paperService } from './exam-paper.service';

@Component({
    selector: 'jhi-exam-paper',
    templateUrl: './exam-paper.component.html'
})
export class Exam_paperComponent implements OnInit, OnDestroy {
    exam_papers: IExam_paper[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected exam_paperService: Exam_paperService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.exam_paperService
            .query()
            .pipe(
                filter((res: HttpResponse<IExam_paper[]>) => res.ok),
                map((res: HttpResponse<IExam_paper[]>) => res.body)
            )
            .subscribe(
                (res: IExam_paper[]) => {
                    this.exam_papers = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInExam_papers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IExam_paper) {
        return item.id;
    }

    registerChangeInExam_papers() {
        this.eventSubscriber = this.eventManager.subscribe('exam_paperListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
