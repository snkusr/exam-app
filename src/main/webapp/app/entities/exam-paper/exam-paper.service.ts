import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IExam_paper } from 'app/shared/model/exam-paper.model';

type EntityResponseType = HttpResponse<IExam_paper>;
type EntityArrayResponseType = HttpResponse<IExam_paper[]>;

@Injectable({ providedIn: 'root' })
export class Exam_paperService {
    public resourceUrl = SERVER_API_URL + 'api/exam-papers';

    constructor(protected http: HttpClient) {}

    create(exam_paper: IExam_paper): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(exam_paper);
        return this.http
            .post<IExam_paper>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(exam_paper: IExam_paper): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(exam_paper);
        return this.http
            .put<IExam_paper>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IExam_paper>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IExam_paper[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(exam_paper: IExam_paper): IExam_paper {
        const copy: IExam_paper = Object.assign({}, exam_paper, {
            creation_date:
                exam_paper.creation_date != null && exam_paper.creation_date.isValid()
                    ? exam_paper.creation_date.format(DATE_FORMAT)
                    : null,
            last_update_date:
                exam_paper.last_update_date != null && exam_paper.last_update_date.isValid()
                    ? exam_paper.last_update_date.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((exam_paper: IExam_paper) => {
                exam_paper.creation_date = exam_paper.creation_date != null ? moment(exam_paper.creation_date) : null;
                exam_paper.last_update_date = exam_paper.last_update_date != null ? moment(exam_paper.last_update_date) : null;
            });
        }
        return res;
    }
}
