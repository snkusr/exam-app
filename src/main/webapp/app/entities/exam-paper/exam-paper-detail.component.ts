import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExam_paper } from 'app/shared/model/exam-paper.model';

@Component({
    selector: 'jhi-exam-paper-detail',
    templateUrl: './exam-paper-detail.component.html'
})
export class Exam_paperDetailComponent implements OnInit {
    exam_paper: IExam_paper;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ exam_paper }) => {
            this.exam_paper = exam_paper;
        });
    }

    previousState() {
        window.history.back();
    }
}
