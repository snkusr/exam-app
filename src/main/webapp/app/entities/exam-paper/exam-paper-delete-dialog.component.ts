import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExam_paper } from 'app/shared/model/exam-paper.model';
import { Exam_paperService } from './exam-paper.service';

@Component({
    selector: 'jhi-exam-paper-delete-dialog',
    templateUrl: './exam-paper-delete-dialog.component.html'
})
export class Exam_paperDeleteDialogComponent {
    exam_paper: IExam_paper;

    constructor(
        protected exam_paperService: Exam_paperService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.exam_paperService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'exam_paperListModification',
                content: 'Deleted an exam_paper'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-exam-paper-delete-popup',
    template: ''
})
export class Exam_paperDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ exam_paper }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(Exam_paperDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.exam_paper = exam_paper;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/exam-paper', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/exam-paper', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
