import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAnswers } from 'app/shared/model/answers.model';
import { AnswersService } from './answers.service';

@Component({
    selector: 'jhi-answers-delete-dialog',
    templateUrl: './answers-delete-dialog.component.html'
})
export class AnswersDeleteDialogComponent {
    answers: IAnswers;

    constructor(protected answersService: AnswersService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.answersService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'answersListModification',
                content: 'Deleted an answers'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-answers-delete-popup',
    template: ''
})
export class AnswersDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ answers }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AnswersDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.answers = answers;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/answers', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/answers', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
