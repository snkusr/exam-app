import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAnswers } from 'app/shared/model/answers.model';
import { AccountService } from 'app/core';
import { AnswersService } from './answers.service';

@Component({
    selector: 'jhi-answers',
    templateUrl: './answers.component.html'
})
export class AnswersComponent implements OnInit, OnDestroy {
    answers: IAnswers[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected answersService: AnswersService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.answersService
            .query()
            .pipe(
                filter((res: HttpResponse<IAnswers[]>) => res.ok),
                map((res: HttpResponse<IAnswers[]>) => res.body)
            )
            .subscribe(
                (res: IAnswers[]) => {
                    this.answers = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAnswers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAnswers) {
        return item.id;
    }

    registerChangeInAnswers() {
        this.eventSubscriber = this.eventManager.subscribe('answersListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
