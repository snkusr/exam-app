import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IAnswers } from 'app/shared/model/answers.model';
import { AnswersService } from './answers.service';
import { IQuestions } from 'app/shared/model/questions.model';
import { QuestionsService } from 'app/entities/questions';
import { IStudents } from 'app/shared/model/students.model';
import { StudentsService } from 'app/entities/students';

@Component({
    selector: 'jhi-answers-update',
    templateUrl: './answers-update.component.html'
})
export class AnswersUpdateComponent implements OnInit {
    answers: IAnswers;
    isSaving: boolean;

    questions: IQuestions[];

    students: IStudents[];
    answered_dateDp: any;
    end_timeDp: any;
    start_timeDp: any;
    creation_dateDp: any;
    last_update_dateDp: any;
    last_updated_byDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected answersService: AnswersService,
        protected questionsService: QuestionsService,
        protected studentsService: StudentsService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ answers }) => {
            this.answers = answers;
        });
        this.questionsService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IQuestions[]>) => mayBeOk.ok),
                map((response: HttpResponse<IQuestions[]>) => response.body)
            )
            .subscribe((res: IQuestions[]) => (this.questions = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.studentsService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IStudents[]>) => mayBeOk.ok),
                map((response: HttpResponse<IStudents[]>) => response.body)
            )
            .subscribe((res: IStudents[]) => (this.students = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.answers.id !== undefined) {
            this.subscribeToSaveResponse(this.answersService.update(this.answers));
        } else {
            this.subscribeToSaveResponse(this.answersService.create(this.answers));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnswers>>) {
        result.subscribe((res: HttpResponse<IAnswers>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackQuestionsById(index: number, item: IQuestions) {
        return item.id;
    }

    trackStudentsById(index: number, item: IStudents) {
        return item.id;
    }
}
