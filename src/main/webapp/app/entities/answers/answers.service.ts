import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAnswers } from 'app/shared/model/answers.model';

type EntityResponseType = HttpResponse<IAnswers>;
type EntityArrayResponseType = HttpResponse<IAnswers[]>;

@Injectable({ providedIn: 'root' })
export class AnswersService {
    public resourceUrl = SERVER_API_URL + 'api/answers';

    constructor(protected http: HttpClient) {}

    create(answers: IAnswers): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(answers);
        return this.http
            .post<IAnswers>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(answers: IAnswers): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(answers);
        return this.http
            .put<IAnswers>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IAnswers>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAnswers[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(answers: IAnswers): IAnswers {
        const copy: IAnswers = Object.assign({}, answers, {
            answered_date:
                answers.answered_date != null && answers.answered_date.isValid() ? answers.answered_date.format(DATE_FORMAT) : null,
            end_time: answers.end_time != null && answers.end_time.isValid() ? answers.end_time.format(DATE_FORMAT) : null,
            start_time: answers.start_time != null && answers.start_time.isValid() ? answers.start_time.format(DATE_FORMAT) : null,
            creation_date:
                answers.creation_date != null && answers.creation_date.isValid() ? answers.creation_date.format(DATE_FORMAT) : null,
            last_update_date:
                answers.last_update_date != null && answers.last_update_date.isValid()
                    ? answers.last_update_date.format(DATE_FORMAT)
                    : null,
            last_updated_by:
                answers.last_updated_by != null && answers.last_updated_by.isValid() ? answers.last_updated_by.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.answered_date = res.body.answered_date != null ? moment(res.body.answered_date) : null;
            res.body.end_time = res.body.end_time != null ? moment(res.body.end_time) : null;
            res.body.start_time = res.body.start_time != null ? moment(res.body.start_time) : null;
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
            res.body.last_updated_by = res.body.last_updated_by != null ? moment(res.body.last_updated_by) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((answers: IAnswers) => {
                answers.answered_date = answers.answered_date != null ? moment(answers.answered_date) : null;
                answers.end_time = answers.end_time != null ? moment(answers.end_time) : null;
                answers.start_time = answers.start_time != null ? moment(answers.start_time) : null;
                answers.creation_date = answers.creation_date != null ? moment(answers.creation_date) : null;
                answers.last_update_date = answers.last_update_date != null ? moment(answers.last_update_date) : null;
                answers.last_updated_by = answers.last_updated_by != null ? moment(answers.last_updated_by) : null;
            });
        }
        return res;
    }
}
