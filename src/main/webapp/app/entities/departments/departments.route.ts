import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Departments } from 'app/shared/model/departments.model';
import { DepartmentsService } from './departments.service';
import { DepartmentsComponent } from './departments.component';
import { DepartmentsDetailComponent } from './departments-detail.component';
import { DepartmentsUpdateComponent } from './departments-update.component';
import { DepartmentsDeletePopupComponent } from './departments-delete-dialog.component';
import { IDepartments } from 'app/shared/model/departments.model';

@Injectable({ providedIn: 'root' })
export class DepartmentsResolve implements Resolve<IDepartments> {
    constructor(private service: DepartmentsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDepartments> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Departments>) => response.ok),
                map((departments: HttpResponse<Departments>) => departments.body)
            );
        }
        return of(new Departments());
    }
}

export const departmentsRoute: Routes = [
    {
        path: '',
        component: DepartmentsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.departments.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: DepartmentsDetailComponent,
        resolve: {
            departments: DepartmentsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.departments.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: DepartmentsUpdateComponent,
        resolve: {
            departments: DepartmentsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.departments.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: DepartmentsUpdateComponent,
        resolve: {
            departments: DepartmentsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.departments.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const departmentsPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: DepartmentsDeletePopupComponent,
        resolve: {
            departments: DepartmentsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.departments.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
