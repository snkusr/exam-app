import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ExamappSharedModule } from 'app/shared';
import {
    DepartmentsComponent,
    DepartmentsDetailComponent,
    DepartmentsUpdateComponent,
    DepartmentsDeletePopupComponent,
    DepartmentsDeleteDialogComponent,
    departmentsRoute,
    departmentsPopupRoute
} from './';

const ENTITY_STATES = [...departmentsRoute, ...departmentsPopupRoute];

@NgModule({
    imports: [ExamappSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DepartmentsComponent,
        DepartmentsDetailComponent,
        DepartmentsUpdateComponent,
        DepartmentsDeleteDialogComponent,
        DepartmentsDeletePopupComponent
    ],
    entryComponents: [DepartmentsComponent, DepartmentsUpdateComponent, DepartmentsDeleteDialogComponent, DepartmentsDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappDepartmentsModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
