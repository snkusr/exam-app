import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { IDepartments } from 'app/shared/model/departments.model';
import { DepartmentsService } from './departments.service';

@Component({
    selector: 'jhi-departments-update',
    templateUrl: './departments-update.component.html'
})
export class DepartmentsUpdateComponent implements OnInit {
    departments: IDepartments;
    isSaving: boolean;
    creation_dateDp: any;
    last_update_dateDp: any;

    constructor(protected departmentsService: DepartmentsService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ departments }) => {
            this.departments = departments;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.departments.id !== undefined) {
            this.subscribeToSaveResponse(this.departmentsService.update(this.departments));
        } else {
            this.subscribeToSaveResponse(this.departmentsService.create(this.departments));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IDepartments>>) {
        result.subscribe((res: HttpResponse<IDepartments>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
