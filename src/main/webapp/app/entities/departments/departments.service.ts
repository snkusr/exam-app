import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDepartments } from 'app/shared/model/departments.model';

type EntityResponseType = HttpResponse<IDepartments>;
type EntityArrayResponseType = HttpResponse<IDepartments[]>;

@Injectable({ providedIn: 'root' })
export class DepartmentsService {
    public resourceUrl = SERVER_API_URL + 'api/departments';

    constructor(protected http: HttpClient) {}

    create(departments: IDepartments): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(departments);
        return this.http
            .post<IDepartments>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(departments: IDepartments): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(departments);
        return this.http
            .put<IDepartments>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IDepartments>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IDepartments[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(departments: IDepartments): IDepartments {
        const copy: IDepartments = Object.assign({}, departments, {
            creation_date:
                departments.creation_date != null && departments.creation_date.isValid()
                    ? departments.creation_date.format(DATE_FORMAT)
                    : null,
            last_update_date:
                departments.last_update_date != null && departments.last_update_date.isValid()
                    ? departments.last_update_date.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((departments: IDepartments) => {
                departments.creation_date = departments.creation_date != null ? moment(departments.creation_date) : null;
                departments.last_update_date = departments.last_update_date != null ? moment(departments.last_update_date) : null;
            });
        }
        return res;
    }
}
