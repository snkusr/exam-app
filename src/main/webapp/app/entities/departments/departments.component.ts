import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDepartments } from 'app/shared/model/departments.model';
import { AccountService } from 'app/core';
import { DepartmentsService } from './departments.service';

@Component({
    selector: 'jhi-departments',
    templateUrl: './departments.component.html'
})
export class DepartmentsComponent implements OnInit, OnDestroy {
    departments: IDepartments[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected departmentsService: DepartmentsService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.departmentsService
            .query()
            .pipe(
                filter((res: HttpResponse<IDepartments[]>) => res.ok),
                map((res: HttpResponse<IDepartments[]>) => res.body)
            )
            .subscribe(
                (res: IDepartments[]) => {
                    this.departments = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDepartments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDepartments) {
        return item.id;
    }

    registerChangeInDepartments() {
        this.eventSubscriber = this.eventManager.subscribe('departmentsListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
