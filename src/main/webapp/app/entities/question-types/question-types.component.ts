import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IQuestion_types } from 'app/shared/model/question-types.model';
import { AccountService } from 'app/core';
import { Question_typesService } from './question-types.service';

@Component({
    selector: 'jhi-question-types',
    templateUrl: './question-types.component.html'
})
export class Question_typesComponent implements OnInit, OnDestroy {
    question_types: IQuestion_types[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected question_typesService: Question_typesService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.question_typesService
            .query()
            .pipe(
                filter((res: HttpResponse<IQuestion_types[]>) => res.ok),
                map((res: HttpResponse<IQuestion_types[]>) => res.body)
            )
            .subscribe(
                (res: IQuestion_types[]) => {
                    this.question_types = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInQuestion_types();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IQuestion_types) {
        return item.id;
    }

    registerChangeInQuestion_types() {
        this.eventSubscriber = this.eventManager.subscribe('question_typesListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
