import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { IQuestion_types } from 'app/shared/model/question-types.model';
import { Question_typesService } from './question-types.service';

@Component({
    selector: 'jhi-question-types-update',
    templateUrl: './question-types-update.component.html'
})
export class Question_typesUpdateComponent implements OnInit {
    question_types: IQuestion_types;
    isSaving: boolean;
    creation_dateDp: any;
    last_update_dateDp: any;

    constructor(protected question_typesService: Question_typesService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ question_types }) => {
            this.question_types = question_types;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.question_types.id !== undefined) {
            this.subscribeToSaveResponse(this.question_typesService.update(this.question_types));
        } else {
            this.subscribeToSaveResponse(this.question_typesService.create(this.question_types));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuestion_types>>) {
        result.subscribe((res: HttpResponse<IQuestion_types>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
