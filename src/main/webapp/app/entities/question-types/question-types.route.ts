import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Question_types } from 'app/shared/model/question-types.model';
import { Question_typesService } from './question-types.service';
import { Question_typesComponent } from './question-types.component';
import { Question_typesDetailComponent } from './question-types-detail.component';
import { Question_typesUpdateComponent } from './question-types-update.component';
import { Question_typesDeletePopupComponent } from './question-types-delete-dialog.component';
import { IQuestion_types } from 'app/shared/model/question-types.model';

@Injectable({ providedIn: 'root' })
export class Question_typesResolve implements Resolve<IQuestion_types> {
    constructor(private service: Question_typesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IQuestion_types> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Question_types>) => response.ok),
                map((question_types: HttpResponse<Question_types>) => question_types.body)
            );
        }
        return of(new Question_types());
    }
}

export const question_typesRoute: Routes = [
    {
        path: '',
        component: Question_typesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.question_types.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: Question_typesDetailComponent,
        resolve: {
            question_types: Question_typesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.question_types.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: Question_typesUpdateComponent,
        resolve: {
            question_types: Question_typesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.question_types.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: Question_typesUpdateComponent,
        resolve: {
            question_types: Question_typesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.question_types.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const question_typesPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: Question_typesDeletePopupComponent,
        resolve: {
            question_types: Question_typesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'examappApp.question_types.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
