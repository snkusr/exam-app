import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IQuestion_types } from 'app/shared/model/question-types.model';

@Component({
    selector: 'jhi-question-types-detail',
    templateUrl: './question-types-detail.component.html'
})
export class Question_typesDetailComponent implements OnInit {
    question_types: IQuestion_types;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ question_types }) => {
            this.question_types = question_types;
        });
    }

    previousState() {
        window.history.back();
    }
}
