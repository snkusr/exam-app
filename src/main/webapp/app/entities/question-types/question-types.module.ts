import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ExamappSharedModule } from 'app/shared';
import {
    Question_typesComponent,
    Question_typesDetailComponent,
    Question_typesUpdateComponent,
    Question_typesDeletePopupComponent,
    Question_typesDeleteDialogComponent,
    question_typesRoute,
    question_typesPopupRoute
} from './';

const ENTITY_STATES = [...question_typesRoute, ...question_typesPopupRoute];

@NgModule({
    imports: [ExamappSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        Question_typesComponent,
        Question_typesDetailComponent,
        Question_typesUpdateComponent,
        Question_typesDeleteDialogComponent,
        Question_typesDeletePopupComponent
    ],
    entryComponents: [
        Question_typesComponent,
        Question_typesUpdateComponent,
        Question_typesDeleteDialogComponent,
        Question_typesDeletePopupComponent
    ],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappQuestion_typesModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
