import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IQuestion_types } from 'app/shared/model/question-types.model';

type EntityResponseType = HttpResponse<IQuestion_types>;
type EntityArrayResponseType = HttpResponse<IQuestion_types[]>;

@Injectable({ providedIn: 'root' })
export class Question_typesService {
    public resourceUrl = SERVER_API_URL + 'api/question-types';

    constructor(protected http: HttpClient) {}

    create(question_types: IQuestion_types): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(question_types);
        return this.http
            .post<IQuestion_types>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(question_types: IQuestion_types): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(question_types);
        return this.http
            .put<IQuestion_types>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IQuestion_types>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IQuestion_types[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(question_types: IQuestion_types): IQuestion_types {
        const copy: IQuestion_types = Object.assign({}, question_types, {
            creation_date:
                question_types.creation_date != null && question_types.creation_date.isValid()
                    ? question_types.creation_date.format(DATE_FORMAT)
                    : null,
            last_update_date:
                question_types.last_update_date != null && question_types.last_update_date.isValid()
                    ? question_types.last_update_date.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((question_types: IQuestion_types) => {
                question_types.creation_date = question_types.creation_date != null ? moment(question_types.creation_date) : null;
                question_types.last_update_date = question_types.last_update_date != null ? moment(question_types.last_update_date) : null;
            });
        }
        return res;
    }
}
