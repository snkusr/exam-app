import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IQuestion_types } from 'app/shared/model/question-types.model';
import { Question_typesService } from './question-types.service';

@Component({
    selector: 'jhi-question-types-delete-dialog',
    templateUrl: './question-types-delete-dialog.component.html'
})
export class Question_typesDeleteDialogComponent {
    question_types: IQuestion_types;

    constructor(
        protected question_typesService: Question_typesService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.question_typesService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'question_typesListModification',
                content: 'Deleted an question_types'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-question-types-delete-popup',
    template: ''
})
export class Question_typesDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ question_types }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(Question_typesDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.question_types = question_types;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/question-types', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/question-types', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
