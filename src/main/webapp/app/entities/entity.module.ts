import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'departments',
                loadChildren: './departments/departments.module#ExamappDepartmentsModule'
            },
            {
                path: 'colleges',
                loadChildren: './colleges/colleges.module#ExamappCollegesModule'
            },
            {
                path: 'question-types',
                loadChildren: './question-types/question-types.module#ExamappQuestion_typesModule'
            },
            {
                path: 'students',
                loadChildren: './students/students.module#ExamappStudentsModule'
            },
            {
                path: 'students',
                loadChildren: './students/students.module#ExamappStudentsModule'
            },
            {
                path: 'students',
                loadChildren: './students/students.module#ExamappStudentsModule'
            },
            {
                path: 'students',
                loadChildren: './students/students.module#ExamappStudentsModule'
            },
            {
                path: 'students',
                loadChildren: './students/students.module#ExamappStudentsModule'
            },
            {
                path: 'contacts',
                loadChildren: './contacts/contacts.module#ExamappContactsModule'
            },
            {
                path: 'questions',
                loadChildren: './questions/questions.module#ExamappQuestionsModule'
            },
            {
                path: 'questions',
                loadChildren: './questions/questions.module#ExamappQuestionsModule'
            },
            {
                path: 'exam-paper',
                loadChildren: './exam-paper/exam-paper.module#ExamappExam_paperModule'
            },
            {
                path: 'answers',
                loadChildren: './answers/answers.module#ExamappAnswersModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappEntityModule {}
