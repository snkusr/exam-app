import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IStudents } from 'app/shared/model/students.model';
import { StudentsService } from './students.service';
import { IColleges } from 'app/shared/model/colleges.model';
import { CollegesService } from 'app/entities/colleges';
import { IDepartments } from 'app/shared/model/departments.model';
import { DepartmentsService } from 'app/entities/departments';

@Component({
    selector: 'jhi-students-update',
    templateUrl: './students-update.component.html'
})
export class StudentsUpdateComponent implements OnInit {
    students: IStudents;
    isSaving: boolean;

    colleges: IColleges[];

    departments: IDepartments[];
    creation_dateDp: any;
    last_update_dateDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected studentsService: StudentsService,
        protected collegesService: CollegesService,
        protected departmentsService: DepartmentsService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ students }) => {
            this.students = students;
        });
        this.collegesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IColleges[]>) => mayBeOk.ok),
                map((response: HttpResponse<IColleges[]>) => response.body)
            )
            .subscribe((res: IColleges[]) => (this.colleges = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.departmentsService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IDepartments[]>) => mayBeOk.ok),
                map((response: HttpResponse<IDepartments[]>) => response.body)
            )
            .subscribe((res: IDepartments[]) => (this.departments = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.students.id !== undefined) {
            this.subscribeToSaveResponse(this.studentsService.update(this.students));
        } else {
            this.subscribeToSaveResponse(this.studentsService.create(this.students));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IStudents>>) {
        result.subscribe((res: HttpResponse<IStudents>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCollegesById(index: number, item: IColleges) {
        return item.id;
    }

    trackDepartmentsById(index: number, item: IDepartments) {
        return item.id;
    }
}
