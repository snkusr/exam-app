import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ExamappSharedModule } from 'app/shared';
import {
    StudentsComponent,
    StudentsDetailComponent,
    StudentsUpdateComponent,
    StudentsDeletePopupComponent,
    StudentsDeleteDialogComponent,
    studentsRoute,
    studentsPopupRoute
} from './';

const ENTITY_STATES = [...studentsRoute, ...studentsPopupRoute];

@NgModule({
    imports: [ExamappSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        StudentsComponent,
        StudentsDetailComponent,
        StudentsUpdateComponent,
        StudentsDeleteDialogComponent,
        StudentsDeletePopupComponent
    ],
    entryComponents: [StudentsComponent, StudentsUpdateComponent, StudentsDeleteDialogComponent, StudentsDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappStudentsModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
