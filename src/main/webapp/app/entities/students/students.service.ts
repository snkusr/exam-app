import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IStudents } from 'app/shared/model/students.model';

type EntityResponseType = HttpResponse<IStudents>;
type EntityArrayResponseType = HttpResponse<IStudents[]>;

@Injectable({ providedIn: 'root' })
export class StudentsService {
    public resourceUrl = SERVER_API_URL + 'api/students';

    constructor(protected http: HttpClient) {}

    create(students: IStudents): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(students);
        return this.http
            .post<IStudents>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(students: IStudents): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(students);
        return this.http
            .put<IStudents>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IStudents>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IStudents[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(students: IStudents): IStudents {
        const copy: IStudents = Object.assign({}, students, {
            creation_date:
                students.creation_date != null && students.creation_date.isValid() ? students.creation_date.format(DATE_FORMAT) : null,
            last_update_date:
                students.last_update_date != null && students.last_update_date.isValid()
                    ? students.last_update_date.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((students: IStudents) => {
                students.creation_date = students.creation_date != null ? moment(students.creation_date) : null;
                students.last_update_date = students.last_update_date != null ? moment(students.last_update_date) : null;
            });
        }
        return res;
    }
}
