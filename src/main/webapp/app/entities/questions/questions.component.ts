import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IQuestions } from 'app/shared/model/questions.model';
import { AccountService } from 'app/core';
import { QuestionsService } from './questions.service';

@Component({
    selector: 'jhi-questions',
    templateUrl: './questions.component.html'
})
export class QuestionsComponent implements OnInit, OnDestroy {
    questions: IQuestions[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected questionsService: QuestionsService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.questionsService
            .query()
            .pipe(
                filter((res: HttpResponse<IQuestions[]>) => res.ok),
                map((res: HttpResponse<IQuestions[]>) => res.body)
            )
            .subscribe(
                (res: IQuestions[]) => {
                    this.questions = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInQuestions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IQuestions) {
        return item.id;
    }

    registerChangeInQuestions() {
        this.eventSubscriber = this.eventManager.subscribe('questionsListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
