import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IQuestions } from 'app/shared/model/questions.model';
import { QuestionsService } from './questions.service';

@Component({
    selector: 'jhi-questions-delete-dialog',
    templateUrl: './questions-delete-dialog.component.html'
})
export class QuestionsDeleteDialogComponent {
    questions: IQuestions;

    constructor(
        protected questionsService: QuestionsService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.questionsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'questionsListModification',
                content: 'Deleted an questions'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-questions-delete-popup',
    template: ''
})
export class QuestionsDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ questions }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(QuestionsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.questions = questions;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/questions', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/questions', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
