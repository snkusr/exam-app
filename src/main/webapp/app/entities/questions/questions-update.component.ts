import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IQuestions } from 'app/shared/model/questions.model';
import { QuestionsService } from './questions.service';
import { IQuestion_types } from 'app/shared/model/question-types.model';
import { Question_typesService } from 'app/entities/question-types';

@Component({
    selector: 'jhi-questions-update',
    templateUrl: './questions-update.component.html'
})
export class QuestionsUpdateComponent implements OnInit {
    questions: IQuestions;
    isSaving: boolean;

    question_types: IQuestion_types[];
    creation_dateDp: any;
    last_update_dateDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected questionsService: QuestionsService,
        protected question_typesService: Question_typesService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ questions }) => {
            this.questions = questions;
        });
        this.question_typesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IQuestion_types[]>) => mayBeOk.ok),
                map((response: HttpResponse<IQuestion_types[]>) => response.body)
            )
            .subscribe((res: IQuestion_types[]) => (this.question_types = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.questions.id !== undefined) {
            this.subscribeToSaveResponse(this.questionsService.update(this.questions));
        } else {
            this.subscribeToSaveResponse(this.questionsService.create(this.questions));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuestions>>) {
        result.subscribe((res: HttpResponse<IQuestions>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackQuestion_typesById(index: number, item: IQuestion_types) {
        return item.id;
    }
}
