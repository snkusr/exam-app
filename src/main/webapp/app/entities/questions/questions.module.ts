import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { ExamappSharedModule } from 'app/shared';
import {
    QuestionsComponent,
    QuestionsDetailComponent,
    QuestionsUpdateComponent,
    QuestionsDeletePopupComponent,
    QuestionsDeleteDialogComponent,
    questionsRoute,
    questionsPopupRoute
} from './';

const ENTITY_STATES = [...questionsRoute, ...questionsPopupRoute];

@NgModule({
    imports: [ExamappSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        QuestionsComponent,
        QuestionsDetailComponent,
        QuestionsUpdateComponent,
        QuestionsDeleteDialogComponent,
        QuestionsDeletePopupComponent
    ],
    entryComponents: [QuestionsComponent, QuestionsUpdateComponent, QuestionsDeleteDialogComponent, QuestionsDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamappQuestionsModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
