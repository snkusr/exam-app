import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IQuestions } from 'app/shared/model/questions.model';

type EntityResponseType = HttpResponse<IQuestions>;
type EntityArrayResponseType = HttpResponse<IQuestions[]>;

@Injectable({ providedIn: 'root' })
export class QuestionsService {
    public resourceUrl = SERVER_API_URL + 'api/questions';

    constructor(protected http: HttpClient) {}

    create(questions: IQuestions): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(questions);
        return this.http
            .post<IQuestions>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(questions: IQuestions): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(questions);
        return this.http
            .put<IQuestions>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IQuestions>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IQuestions[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(questions: IQuestions): IQuestions {
        const copy: IQuestions = Object.assign({}, questions, {
            creation_date:
                questions.creation_date != null && questions.creation_date.isValid() ? questions.creation_date.format(DATE_FORMAT) : null,
            last_update_date:
                questions.last_update_date != null && questions.last_update_date.isValid()
                    ? questions.last_update_date.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.creation_date = res.body.creation_date != null ? moment(res.body.creation_date) : null;
            res.body.last_update_date = res.body.last_update_date != null ? moment(res.body.last_update_date) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((questions: IQuestions) => {
                questions.creation_date = questions.creation_date != null ? moment(questions.creation_date) : null;
                questions.last_update_date = questions.last_update_date != null ? moment(questions.last_update_date) : null;
            });
        }
        return res;
    }
}
