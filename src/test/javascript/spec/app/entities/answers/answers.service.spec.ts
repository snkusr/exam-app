/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { AnswersService } from 'app/entities/answers/answers.service';
import { IAnswers, Answers } from 'app/shared/model/answers.model';

describe('Service Tests', () => {
    describe('Answers Service', () => {
        let injector: TestBed;
        let service: AnswersService;
        let httpMock: HttpTestingController;
        let elemDefault: IAnswers;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(AnswersService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Answers(
                0,
                'AAAAAAA',
                currentDate,
                currentDate,
                currentDate,
                currentDate,
                'AAAAAAA',
                currentDate,
                currentDate
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        answered_date: currentDate.format(DATE_FORMAT),
                        end_time: currentDate.format(DATE_FORMAT),
                        start_time: currentDate.format(DATE_FORMAT),
                        creation_date: currentDate.format(DATE_FORMAT),
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Answers', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        answered_date: currentDate.format(DATE_FORMAT),
                        end_time: currentDate.format(DATE_FORMAT),
                        start_time: currentDate.format(DATE_FORMAT),
                        creation_date: currentDate.format(DATE_FORMAT),
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        answered_date: currentDate,
                        end_time: currentDate,
                        start_time: currentDate,
                        creation_date: currentDate,
                        last_update_date: currentDate,
                        last_updated_by: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Answers(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Answers', async () => {
                const returnedFromService = Object.assign(
                    {
                        answer: 'BBBBBB',
                        answered_date: currentDate.format(DATE_FORMAT),
                        end_time: currentDate.format(DATE_FORMAT),
                        start_time: currentDate.format(DATE_FORMAT),
                        creation_date: currentDate.format(DATE_FORMAT),
                        created_by: 'BBBBBB',
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        answered_date: currentDate,
                        end_time: currentDate,
                        start_time: currentDate,
                        creation_date: currentDate,
                        last_update_date: currentDate,
                        last_updated_by: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Answers', async () => {
                const returnedFromService = Object.assign(
                    {
                        answer: 'BBBBBB',
                        answered_date: currentDate.format(DATE_FORMAT),
                        end_time: currentDate.format(DATE_FORMAT),
                        start_time: currentDate.format(DATE_FORMAT),
                        creation_date: currentDate.format(DATE_FORMAT),
                        created_by: 'BBBBBB',
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        answered_date: currentDate,
                        end_time: currentDate,
                        start_time: currentDate,
                        creation_date: currentDate,
                        last_update_date: currentDate,
                        last_updated_by: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Answers', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
