/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { Question_typesService } from 'app/entities/question-types/question-types.service';
import { IQuestion_types, Question_types } from 'app/shared/model/question-types.model';

describe('Service Tests', () => {
    describe('Question_types Service', () => {
        let injector: TestBed;
        let service: Question_typesService;
        let httpMock: HttpTestingController;
        let elemDefault: IQuestion_types;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(Question_typesService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Question_types(0, 'AAAAAAA', 'AAAAAAA', false, currentDate, 'AAAAAAA', currentDate, 'AAAAAAA');
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        creation_date: currentDate.format(DATE_FORMAT),
                        last_update_date: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Question_types', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        creation_date: currentDate.format(DATE_FORMAT),
                        last_update_date: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        creation_date: currentDate,
                        last_update_date: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Question_types(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Question_types', async () => {
                const returnedFromService = Object.assign(
                    {
                        type_code: 'BBBBBB',
                        type_name: 'BBBBBB',
                        status: true,
                        creation_date: currentDate.format(DATE_FORMAT),
                        created_by: 'BBBBBB',
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        creation_date: currentDate,
                        last_update_date: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Question_types', async () => {
                const returnedFromService = Object.assign(
                    {
                        type_code: 'BBBBBB',
                        type_name: 'BBBBBB',
                        status: true,
                        creation_date: currentDate.format(DATE_FORMAT),
                        created_by: 'BBBBBB',
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        creation_date: currentDate,
                        last_update_date: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Question_types', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
