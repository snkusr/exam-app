/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ExamappTestModule } from '../../../test.module';
import { Question_typesDeleteDialogComponent } from 'app/entities/question-types/question-types-delete-dialog.component';
import { Question_typesService } from 'app/entities/question-types/question-types.service';

describe('Component Tests', () => {
    describe('Question_types Management Delete Component', () => {
        let comp: Question_typesDeleteDialogComponent;
        let fixture: ComponentFixture<Question_typesDeleteDialogComponent>;
        let service: Question_typesService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [Question_typesDeleteDialogComponent]
            })
                .overrideTemplate(Question_typesDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(Question_typesDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Question_typesService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
