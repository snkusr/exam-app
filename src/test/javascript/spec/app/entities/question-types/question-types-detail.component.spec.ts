/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { Question_typesDetailComponent } from 'app/entities/question-types/question-types-detail.component';
import { Question_types } from 'app/shared/model/question-types.model';

describe('Component Tests', () => {
    describe('Question_types Management Detail Component', () => {
        let comp: Question_typesDetailComponent;
        let fixture: ComponentFixture<Question_typesDetailComponent>;
        const route = ({ data: of({ question_types: new Question_types(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [Question_typesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(Question_typesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(Question_typesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.question_types).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
