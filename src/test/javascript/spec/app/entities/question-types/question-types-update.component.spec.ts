/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { Question_typesUpdateComponent } from 'app/entities/question-types/question-types-update.component';
import { Question_typesService } from 'app/entities/question-types/question-types.service';
import { Question_types } from 'app/shared/model/question-types.model';

describe('Component Tests', () => {
    describe('Question_types Management Update Component', () => {
        let comp: Question_typesUpdateComponent;
        let fixture: ComponentFixture<Question_typesUpdateComponent>;
        let service: Question_typesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [Question_typesUpdateComponent]
            })
                .overrideTemplate(Question_typesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Question_typesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Question_typesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Question_types(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.question_types = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Question_types();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.question_types = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
