/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ExamappTestModule } from '../../../test.module';
import { Question_typesComponent } from 'app/entities/question-types/question-types.component';
import { Question_typesService } from 'app/entities/question-types/question-types.service';
import { Question_types } from 'app/shared/model/question-types.model';

describe('Component Tests', () => {
    describe('Question_types Management Component', () => {
        let comp: Question_typesComponent;
        let fixture: ComponentFixture<Question_typesComponent>;
        let service: Question_typesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [Question_typesComponent],
                providers: []
            })
                .overrideTemplate(Question_typesComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Question_typesComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Question_typesService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Question_types(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.question_types[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
