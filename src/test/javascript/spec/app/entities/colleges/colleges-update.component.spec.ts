/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { CollegesUpdateComponent } from 'app/entities/colleges/colleges-update.component';
import { CollegesService } from 'app/entities/colleges/colleges.service';
import { Colleges } from 'app/shared/model/colleges.model';

describe('Component Tests', () => {
    describe('Colleges Management Update Component', () => {
        let comp: CollegesUpdateComponent;
        let fixture: ComponentFixture<CollegesUpdateComponent>;
        let service: CollegesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [CollegesUpdateComponent]
            })
                .overrideTemplate(CollegesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CollegesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CollegesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Colleges(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.colleges = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Colleges();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.colleges = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
