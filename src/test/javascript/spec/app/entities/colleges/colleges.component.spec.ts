/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ExamappTestModule } from '../../../test.module';
import { CollegesComponent } from 'app/entities/colleges/colleges.component';
import { CollegesService } from 'app/entities/colleges/colleges.service';
import { Colleges } from 'app/shared/model/colleges.model';

describe('Component Tests', () => {
    describe('Colleges Management Component', () => {
        let comp: CollegesComponent;
        let fixture: ComponentFixture<CollegesComponent>;
        let service: CollegesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [CollegesComponent],
                providers: []
            })
                .overrideTemplate(CollegesComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CollegesComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CollegesService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Colleges(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.colleges[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
