/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { CollegesDetailComponent } from 'app/entities/colleges/colleges-detail.component';
import { Colleges } from 'app/shared/model/colleges.model';

describe('Component Tests', () => {
    describe('Colleges Management Detail Component', () => {
        let comp: CollegesDetailComponent;
        let fixture: ComponentFixture<CollegesDetailComponent>;
        const route = ({ data: of({ colleges: new Colleges(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [CollegesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CollegesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CollegesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.colleges).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
