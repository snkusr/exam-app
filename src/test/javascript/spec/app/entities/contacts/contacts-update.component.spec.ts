/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { ContactsUpdateComponent } from 'app/entities/contacts/contacts-update.component';
import { ContactsService } from 'app/entities/contacts/contacts.service';
import { Contacts } from 'app/shared/model/contacts.model';

describe('Component Tests', () => {
    describe('Contacts Management Update Component', () => {
        let comp: ContactsUpdateComponent;
        let fixture: ComponentFixture<ContactsUpdateComponent>;
        let service: ContactsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [ContactsUpdateComponent]
            })
                .overrideTemplate(ContactsUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ContactsUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ContactsService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Contacts(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.contacts = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Contacts();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.contacts = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
