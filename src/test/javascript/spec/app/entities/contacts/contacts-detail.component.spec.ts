/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { ContactsDetailComponent } from 'app/entities/contacts/contacts-detail.component';
import { Contacts } from 'app/shared/model/contacts.model';

describe('Component Tests', () => {
    describe('Contacts Management Detail Component', () => {
        let comp: ContactsDetailComponent;
        let fixture: ComponentFixture<ContactsDetailComponent>;
        const route = ({ data: of({ contacts: new Contacts(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [ContactsDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ContactsDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ContactsDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.contacts).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
