/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ExamappTestModule } from '../../../test.module';
import { ContactsComponent } from 'app/entities/contacts/contacts.component';
import { ContactsService } from 'app/entities/contacts/contacts.service';
import { Contacts } from 'app/shared/model/contacts.model';

describe('Component Tests', () => {
    describe('Contacts Management Component', () => {
        let comp: ContactsComponent;
        let fixture: ComponentFixture<ContactsComponent>;
        let service: ContactsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [ContactsComponent],
                providers: []
            })
                .overrideTemplate(ContactsComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ContactsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ContactsService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Contacts(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.contacts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
