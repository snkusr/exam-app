/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ExamappTestModule } from '../../../test.module';
import { ContactsDeleteDialogComponent } from 'app/entities/contacts/contacts-delete-dialog.component';
import { ContactsService } from 'app/entities/contacts/contacts.service';

describe('Component Tests', () => {
    describe('Contacts Management Delete Component', () => {
        let comp: ContactsDeleteDialogComponent;
        let fixture: ComponentFixture<ContactsDeleteDialogComponent>;
        let service: ContactsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [ContactsDeleteDialogComponent]
            })
                .overrideTemplate(ContactsDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ContactsDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ContactsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
