/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ExamappTestModule } from '../../../test.module';
import { Exam_paperComponent } from 'app/entities/exam-paper/exam-paper.component';
import { Exam_paperService } from 'app/entities/exam-paper/exam-paper.service';
import { Exam_paper } from 'app/shared/model/exam-paper.model';

describe('Component Tests', () => {
    describe('Exam_paper Management Component', () => {
        let comp: Exam_paperComponent;
        let fixture: ComponentFixture<Exam_paperComponent>;
        let service: Exam_paperService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [Exam_paperComponent],
                providers: []
            })
                .overrideTemplate(Exam_paperComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Exam_paperComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Exam_paperService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Exam_paper(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.exam_papers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
