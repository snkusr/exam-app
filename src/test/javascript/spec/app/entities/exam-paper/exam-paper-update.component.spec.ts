/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { Exam_paperUpdateComponent } from 'app/entities/exam-paper/exam-paper-update.component';
import { Exam_paperService } from 'app/entities/exam-paper/exam-paper.service';
import { Exam_paper } from 'app/shared/model/exam-paper.model';

describe('Component Tests', () => {
    describe('Exam_paper Management Update Component', () => {
        let comp: Exam_paperUpdateComponent;
        let fixture: ComponentFixture<Exam_paperUpdateComponent>;
        let service: Exam_paperService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [Exam_paperUpdateComponent]
            })
                .overrideTemplate(Exam_paperUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(Exam_paperUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(Exam_paperService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Exam_paper(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.exam_paper = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Exam_paper();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.exam_paper = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
