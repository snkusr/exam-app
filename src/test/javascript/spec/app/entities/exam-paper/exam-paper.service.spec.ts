/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { Exam_paperService } from 'app/entities/exam-paper/exam-paper.service';
import { IExam_paper, Exam_paper } from 'app/shared/model/exam-paper.model';

describe('Service Tests', () => {
    describe('Exam_paper Service', () => {
        let injector: TestBed;
        let service: Exam_paperService;
        let httpMock: HttpTestingController;
        let elemDefault: IExam_paper;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(Exam_paperService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new Exam_paper(0, 'AAAAAAA', currentDate, 'AAAAAAA', currentDate, 'AAAAAAA');
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        creation_date: currentDate.format(DATE_FORMAT),
                        last_update_date: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a Exam_paper', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        creation_date: currentDate.format(DATE_FORMAT),
                        last_update_date: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        creation_date: currentDate,
                        last_update_date: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new Exam_paper(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a Exam_paper', async () => {
                const returnedFromService = Object.assign(
                    {
                        name: 'BBBBBB',
                        creation_date: currentDate.format(DATE_FORMAT),
                        created_by: 'BBBBBB',
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: 'BBBBBB'
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        creation_date: currentDate,
                        last_update_date: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of Exam_paper', async () => {
                const returnedFromService = Object.assign(
                    {
                        name: 'BBBBBB',
                        creation_date: currentDate.format(DATE_FORMAT),
                        created_by: 'BBBBBB',
                        last_update_date: currentDate.format(DATE_FORMAT),
                        last_updated_by: 'BBBBBB'
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        creation_date: currentDate,
                        last_update_date: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a Exam_paper', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
