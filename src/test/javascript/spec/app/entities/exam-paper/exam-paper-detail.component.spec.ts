/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExamappTestModule } from '../../../test.module';
import { Exam_paperDetailComponent } from 'app/entities/exam-paper/exam-paper-detail.component';
import { Exam_paper } from 'app/shared/model/exam-paper.model';

describe('Component Tests', () => {
    describe('Exam_paper Management Detail Component', () => {
        let comp: Exam_paperDetailComponent;
        let fixture: ComponentFixture<Exam_paperDetailComponent>;
        const route = ({ data: of({ exam_paper: new Exam_paper(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [ExamappTestModule],
                declarations: [Exam_paperDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(Exam_paperDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(Exam_paperDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.exam_paper).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
