package com.snithik.examapp.web.rest;

import com.snithik.examapp.ExamappApp;

import com.snithik.examapp.domain.Answers;
import com.snithik.examapp.repository.AnswersRepository;
import com.snithik.examapp.service.AnswersService;
import com.snithik.examapp.service.dto.AnswersDTO;
import com.snithik.examapp.service.mapper.AnswersMapper;
import com.snithik.examapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.snithik.examapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AnswersResource REST controller.
 *
 * @see AnswersResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExamappApp.class)
public class AnswersResourceIntTest {

    private static final String DEFAULT_ANSWER = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_ANSWERED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ANSWERED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_TIME = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_TIME = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_START_TIME = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_TIME = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LAST_UPDATED_BY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_BY = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private AnswersRepository answersRepository;

    @Autowired
    private AnswersMapper answersMapper;

    @Autowired
    private AnswersService answersService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAnswersMockMvc;

    private Answers answers;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AnswersResource answersResource = new AnswersResource(answersService);
        this.restAnswersMockMvc = MockMvcBuilders.standaloneSetup(answersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Answers createEntity(EntityManager em) {
        Answers answers = new Answers()
            .answer(DEFAULT_ANSWER)
            .answered_date(DEFAULT_ANSWERED_DATE)
            .end_time(DEFAULT_END_TIME)
            .start_time(DEFAULT_START_TIME)
            .creation_date(DEFAULT_CREATION_DATE)
            .created_by(DEFAULT_CREATED_BY)
            .last_update_date(DEFAULT_LAST_UPDATE_DATE)
            .last_updated_by(DEFAULT_LAST_UPDATED_BY);
        return answers;
    }

    @Before
    public void initTest() {
        answers = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnswers() throws Exception {
        int databaseSizeBeforeCreate = answersRepository.findAll().size();

        // Create the Answers
        AnswersDTO answersDTO = answersMapper.toDto(answers);
        restAnswersMockMvc.perform(post("/api/answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answersDTO)))
            .andExpect(status().isCreated());

        // Validate the Answers in the database
        List<Answers> answersList = answersRepository.findAll();
        assertThat(answersList).hasSize(databaseSizeBeforeCreate + 1);
        Answers testAnswers = answersList.get(answersList.size() - 1);
        assertThat(testAnswers.getAnswer()).isEqualTo(DEFAULT_ANSWER);
        assertThat(testAnswers.getAnswered_date()).isEqualTo(DEFAULT_ANSWERED_DATE);
        assertThat(testAnswers.getEnd_time()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testAnswers.getStart_time()).isEqualTo(DEFAULT_START_TIME);
        assertThat(testAnswers.getCreation_date()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testAnswers.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testAnswers.getLast_update_date()).isEqualTo(DEFAULT_LAST_UPDATE_DATE);
        assertThat(testAnswers.getLast_updated_by()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createAnswersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = answersRepository.findAll().size();

        // Create the Answers with an existing ID
        answers.setId(1L);
        AnswersDTO answersDTO = answersMapper.toDto(answers);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnswersMockMvc.perform(post("/api/answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Answers in the database
        List<Answers> answersList = answersRepository.findAll();
        assertThat(answersList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAnswers() throws Exception {
        // Initialize the database
        answersRepository.saveAndFlush(answers);

        // Get all the answersList
        restAnswersMockMvc.perform(get("/api/answers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(answers.getId().intValue())))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER.toString())))
            .andExpect(jsonPath("$.[*].answered_date").value(hasItem(DEFAULT_ANSWERED_DATE.toString())))
            .andExpect(jsonPath("$.[*].end_time").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].start_time").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].last_update_date").value(hasItem(DEFAULT_LAST_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].last_updated_by").value(hasItem(DEFAULT_LAST_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getAnswers() throws Exception {
        // Initialize the database
        answersRepository.saveAndFlush(answers);

        // Get the answers
        restAnswersMockMvc.perform(get("/api/answers/{id}", answers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(answers.getId().intValue()))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER.toString()))
            .andExpect(jsonPath("$.answered_date").value(DEFAULT_ANSWERED_DATE.toString()))
            .andExpect(jsonPath("$.end_time").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.start_time").value(DEFAULT_START_TIME.toString()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.last_update_date").value(DEFAULT_LAST_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.last_updated_by").value(DEFAULT_LAST_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAnswers() throws Exception {
        // Get the answers
        restAnswersMockMvc.perform(get("/api/answers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnswers() throws Exception {
        // Initialize the database
        answersRepository.saveAndFlush(answers);

        int databaseSizeBeforeUpdate = answersRepository.findAll().size();

        // Update the answers
        Answers updatedAnswers = answersRepository.findById(answers.getId()).get();
        // Disconnect from session so that the updates on updatedAnswers are not directly saved in db
        em.detach(updatedAnswers);
        updatedAnswers
            .answer(UPDATED_ANSWER)
            .answered_date(UPDATED_ANSWERED_DATE)
            .end_time(UPDATED_END_TIME)
            .start_time(UPDATED_START_TIME)
            .creation_date(UPDATED_CREATION_DATE)
            .created_by(UPDATED_CREATED_BY)
            .last_update_date(UPDATED_LAST_UPDATE_DATE)
            .last_updated_by(UPDATED_LAST_UPDATED_BY);
        AnswersDTO answersDTO = answersMapper.toDto(updatedAnswers);

        restAnswersMockMvc.perform(put("/api/answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answersDTO)))
            .andExpect(status().isOk());

        // Validate the Answers in the database
        List<Answers> answersList = answersRepository.findAll();
        assertThat(answersList).hasSize(databaseSizeBeforeUpdate);
        Answers testAnswers = answersList.get(answersList.size() - 1);
        assertThat(testAnswers.getAnswer()).isEqualTo(UPDATED_ANSWER);
        assertThat(testAnswers.getAnswered_date()).isEqualTo(UPDATED_ANSWERED_DATE);
        assertThat(testAnswers.getEnd_time()).isEqualTo(UPDATED_END_TIME);
        assertThat(testAnswers.getStart_time()).isEqualTo(UPDATED_START_TIME);
        assertThat(testAnswers.getCreation_date()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testAnswers.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testAnswers.getLast_update_date()).isEqualTo(UPDATED_LAST_UPDATE_DATE);
        assertThat(testAnswers.getLast_updated_by()).isEqualTo(UPDATED_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingAnswers() throws Exception {
        int databaseSizeBeforeUpdate = answersRepository.findAll().size();

        // Create the Answers
        AnswersDTO answersDTO = answersMapper.toDto(answers);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnswersMockMvc.perform(put("/api/answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(answersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Answers in the database
        List<Answers> answersList = answersRepository.findAll();
        assertThat(answersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnswers() throws Exception {
        // Initialize the database
        answersRepository.saveAndFlush(answers);

        int databaseSizeBeforeDelete = answersRepository.findAll().size();

        // Delete the answers
        restAnswersMockMvc.perform(delete("/api/answers/{id}", answers.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Answers> answersList = answersRepository.findAll();
        assertThat(answersList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Answers.class);
        Answers answers1 = new Answers();
        answers1.setId(1L);
        Answers answers2 = new Answers();
        answers2.setId(answers1.getId());
        assertThat(answers1).isEqualTo(answers2);
        answers2.setId(2L);
        assertThat(answers1).isNotEqualTo(answers2);
        answers1.setId(null);
        assertThat(answers1).isNotEqualTo(answers2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnswersDTO.class);
        AnswersDTO answersDTO1 = new AnswersDTO();
        answersDTO1.setId(1L);
        AnswersDTO answersDTO2 = new AnswersDTO();
        assertThat(answersDTO1).isNotEqualTo(answersDTO2);
        answersDTO2.setId(answersDTO1.getId());
        assertThat(answersDTO1).isEqualTo(answersDTO2);
        answersDTO2.setId(2L);
        assertThat(answersDTO1).isNotEqualTo(answersDTO2);
        answersDTO1.setId(null);
        assertThat(answersDTO1).isNotEqualTo(answersDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(answersMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(answersMapper.fromId(null)).isNull();
    }
}
