package com.snithik.examapp.web.rest;

import com.snithik.examapp.ExamappApp;

import com.snithik.examapp.domain.Departments;
import com.snithik.examapp.repository.DepartmentsRepository;
import com.snithik.examapp.service.DepartmentsService;
import com.snithik.examapp.service.dto.DepartmentsDTO;
import com.snithik.examapp.service.mapper.DepartmentsMapper;
import com.snithik.examapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.snithik.examapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DepartmentsResource REST controller.
 *
 * @see DepartmentsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExamappApp.class)
public class DepartmentsResourceIntTest {

    private static final String DEFAULT_DEPARTMENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_CODE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private DepartmentsRepository departmentsRepository;

    @Autowired
    private DepartmentsMapper departmentsMapper;

    @Autowired
    private DepartmentsService departmentsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDepartmentsMockMvc;

    private Departments departments;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DepartmentsResource departmentsResource = new DepartmentsResource(departmentsService);
        this.restDepartmentsMockMvc = MockMvcBuilders.standaloneSetup(departmentsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Departments createEntity(EntityManager em) {
        Departments departments = new Departments()
            .department_name(DEFAULT_DEPARTMENT_NAME)
            .department_code(DEFAULT_DEPARTMENT_CODE)
            .status(DEFAULT_STATUS)
            .creation_date(DEFAULT_CREATION_DATE)
            .created_by(DEFAULT_CREATED_BY)
            .last_update_date(DEFAULT_LAST_UPDATE_DATE)
            .last_updated_by(DEFAULT_LAST_UPDATED_BY);
        return departments;
    }

    @Before
    public void initTest() {
        departments = createEntity(em);
    }

    @Test
    @Transactional
    public void createDepartments() throws Exception {
        int databaseSizeBeforeCreate = departmentsRepository.findAll().size();

        // Create the Departments
        DepartmentsDTO departmentsDTO = departmentsMapper.toDto(departments);
        restDepartmentsMockMvc.perform(post("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentsDTO)))
            .andExpect(status().isCreated());

        // Validate the Departments in the database
        List<Departments> departmentsList = departmentsRepository.findAll();
        assertThat(departmentsList).hasSize(databaseSizeBeforeCreate + 1);
        Departments testDepartments = departmentsList.get(departmentsList.size() - 1);
        assertThat(testDepartments.getDepartment_name()).isEqualTo(DEFAULT_DEPARTMENT_NAME);
        assertThat(testDepartments.getDepartment_code()).isEqualTo(DEFAULT_DEPARTMENT_CODE);
        assertThat(testDepartments.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDepartments.getCreation_date()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testDepartments.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testDepartments.getLast_update_date()).isEqualTo(DEFAULT_LAST_UPDATE_DATE);
        assertThat(testDepartments.getLast_updated_by()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createDepartmentsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = departmentsRepository.findAll().size();

        // Create the Departments with an existing ID
        departments.setId(1L);
        DepartmentsDTO departmentsDTO = departmentsMapper.toDto(departments);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDepartmentsMockMvc.perform(post("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Departments in the database
        List<Departments> departmentsList = departmentsRepository.findAll();
        assertThat(departmentsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDepartments() throws Exception {
        // Initialize the database
        departmentsRepository.saveAndFlush(departments);

        // Get all the departmentsList
        restDepartmentsMockMvc.perform(get("/api/departments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(departments.getId().intValue())))
            .andExpect(jsonPath("$.[*].department_name").value(hasItem(DEFAULT_DEPARTMENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].department_code").value(hasItem(DEFAULT_DEPARTMENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].last_update_date").value(hasItem(DEFAULT_LAST_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].last_updated_by").value(hasItem(DEFAULT_LAST_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getDepartments() throws Exception {
        // Initialize the database
        departmentsRepository.saveAndFlush(departments);

        // Get the departments
        restDepartmentsMockMvc.perform(get("/api/departments/{id}", departments.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(departments.getId().intValue()))
            .andExpect(jsonPath("$.department_name").value(DEFAULT_DEPARTMENT_NAME.toString()))
            .andExpect(jsonPath("$.department_code").value(DEFAULT_DEPARTMENT_CODE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.last_update_date").value(DEFAULT_LAST_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.last_updated_by").value(DEFAULT_LAST_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDepartments() throws Exception {
        // Get the departments
        restDepartmentsMockMvc.perform(get("/api/departments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDepartments() throws Exception {
        // Initialize the database
        departmentsRepository.saveAndFlush(departments);

        int databaseSizeBeforeUpdate = departmentsRepository.findAll().size();

        // Update the departments
        Departments updatedDepartments = departmentsRepository.findById(departments.getId()).get();
        // Disconnect from session so that the updates on updatedDepartments are not directly saved in db
        em.detach(updatedDepartments);
        updatedDepartments
            .department_name(UPDATED_DEPARTMENT_NAME)
            .department_code(UPDATED_DEPARTMENT_CODE)
            .status(UPDATED_STATUS)
            .creation_date(UPDATED_CREATION_DATE)
            .created_by(UPDATED_CREATED_BY)
            .last_update_date(UPDATED_LAST_UPDATE_DATE)
            .last_updated_by(UPDATED_LAST_UPDATED_BY);
        DepartmentsDTO departmentsDTO = departmentsMapper.toDto(updatedDepartments);

        restDepartmentsMockMvc.perform(put("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentsDTO)))
            .andExpect(status().isOk());

        // Validate the Departments in the database
        List<Departments> departmentsList = departmentsRepository.findAll();
        assertThat(departmentsList).hasSize(databaseSizeBeforeUpdate);
        Departments testDepartments = departmentsList.get(departmentsList.size() - 1);
        assertThat(testDepartments.getDepartment_name()).isEqualTo(UPDATED_DEPARTMENT_NAME);
        assertThat(testDepartments.getDepartment_code()).isEqualTo(UPDATED_DEPARTMENT_CODE);
        assertThat(testDepartments.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDepartments.getCreation_date()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testDepartments.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDepartments.getLast_update_date()).isEqualTo(UPDATED_LAST_UPDATE_DATE);
        assertThat(testDepartments.getLast_updated_by()).isEqualTo(UPDATED_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingDepartments() throws Exception {
        int databaseSizeBeforeUpdate = departmentsRepository.findAll().size();

        // Create the Departments
        DepartmentsDTO departmentsDTO = departmentsMapper.toDto(departments);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDepartmentsMockMvc.perform(put("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Departments in the database
        List<Departments> departmentsList = departmentsRepository.findAll();
        assertThat(departmentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDepartments() throws Exception {
        // Initialize the database
        departmentsRepository.saveAndFlush(departments);

        int databaseSizeBeforeDelete = departmentsRepository.findAll().size();

        // Delete the departments
        restDepartmentsMockMvc.perform(delete("/api/departments/{id}", departments.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Departments> departmentsList = departmentsRepository.findAll();
        assertThat(departmentsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Departments.class);
        Departments departments1 = new Departments();
        departments1.setId(1L);
        Departments departments2 = new Departments();
        departments2.setId(departments1.getId());
        assertThat(departments1).isEqualTo(departments2);
        departments2.setId(2L);
        assertThat(departments1).isNotEqualTo(departments2);
        departments1.setId(null);
        assertThat(departments1).isNotEqualTo(departments2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DepartmentsDTO.class);
        DepartmentsDTO departmentsDTO1 = new DepartmentsDTO();
        departmentsDTO1.setId(1L);
        DepartmentsDTO departmentsDTO2 = new DepartmentsDTO();
        assertThat(departmentsDTO1).isNotEqualTo(departmentsDTO2);
        departmentsDTO2.setId(departmentsDTO1.getId());
        assertThat(departmentsDTO1).isEqualTo(departmentsDTO2);
        departmentsDTO2.setId(2L);
        assertThat(departmentsDTO1).isNotEqualTo(departmentsDTO2);
        departmentsDTO1.setId(null);
        assertThat(departmentsDTO1).isNotEqualTo(departmentsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(departmentsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(departmentsMapper.fromId(null)).isNull();
    }
}
