package com.snithik.examapp.web.rest;

import com.snithik.examapp.ExamappApp;

import com.snithik.examapp.domain.Students;
import com.snithik.examapp.repository.StudentsRepository;
import com.snithik.examapp.service.StudentsService;
import com.snithik.examapp.service.dto.StudentsDTO;
import com.snithik.examapp.service.mapper.StudentsMapper;
import com.snithik.examapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.snithik.examapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StudentsResource REST controller.
 *
 * @see StudentsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExamappApp.class)
public class StudentsResourceIntTest {

    private static final String DEFAULT_STUDENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STUDENT_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_MOBILE = 1L;
    private static final Long UPDATED_MOBILE = 2L;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_STUDYING_YEAR = "AAAAAAAAAA";
    private static final String UPDATED_STUDYING_YEAR = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final Long DEFAULT_EMCETECET_RANK = 1L;
    private static final Long UPDATED_EMCETECET_RANK = 2L;

    private static final Float DEFAULT_CURRENT_CGPA = 1F;
    private static final Float UPDATED_CURRENT_CGPA = 2F;

    private static final String DEFAULT_FATHERS_OCCUPATION = "AAAAAAAAAA";
    private static final String UPDATED_FATHERS_OCCUPATION = "BBBBBBBBBB";

    private static final String DEFAULT_MOTHERS_OCCUPATION = "AAAAAAAAAA";
    private static final String UPDATED_MOTHERS_OCCUPATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_SIBLINGS = 1;
    private static final Integer UPDATED_SIBLINGS = 2;

    private static final String DEFAULT_FAMILY_INCOME = "AAAAAAAAAA";
    private static final String UPDATED_FAMILY_INCOME = "BBBBBBBBBB";

    private static final String DEFAULT_NATIVE_PLACE = "AAAAAAAAAA";
    private static final String UPDATED_NATIVE_PLACE = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGES_KNOWN = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGES_KNOWN = "BBBBBBBBBB";

    @Autowired
    private StudentsRepository studentsRepository;

    @Autowired
    private StudentsMapper studentsMapper;

    @Autowired
    private StudentsService studentsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restStudentsMockMvc;

    private Students students;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StudentsResource studentsResource = new StudentsResource(studentsService);
        this.restStudentsMockMvc = MockMvcBuilders.standaloneSetup(studentsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Students createEntity(EntityManager em) {
        Students students = new Students()
            .student_name(DEFAULT_STUDENT_NAME)
            .mobile(DEFAULT_MOBILE)
            .email(DEFAULT_EMAIL)
            .studying_year(DEFAULT_STUDYING_YEAR)
            .status(DEFAULT_STATUS)
            .creation_date(DEFAULT_CREATION_DATE)
            .created_by(DEFAULT_CREATED_BY)
            .last_update_date(DEFAULT_LAST_UPDATE_DATE)
            .last_updated_by(DEFAULT_LAST_UPDATED_BY)
            .department(DEFAULT_DEPARTMENT)
            .emcetecet_rank(DEFAULT_EMCETECET_RANK)
            .current_CGPA(DEFAULT_CURRENT_CGPA)
            .fathers_occupation(DEFAULT_FATHERS_OCCUPATION)
            .mothers_occupation(DEFAULT_MOTHERS_OCCUPATION)
            .siblings(DEFAULT_SIBLINGS)
            .family_income(DEFAULT_FAMILY_INCOME)
            .native_place(DEFAULT_NATIVE_PLACE)
            .languages_known(DEFAULT_LANGUAGES_KNOWN);
        return students;
    }

    @Before
    public void initTest() {
        students = createEntity(em);
    }

    @Test
    @Transactional
    public void createStudents() throws Exception {
        int databaseSizeBeforeCreate = studentsRepository.findAll().size();

        // Create the Students
        StudentsDTO studentsDTO = studentsMapper.toDto(students);
        restStudentsMockMvc.perform(post("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentsDTO)))
            .andExpect(status().isCreated());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeCreate + 1);
        Students testStudents = studentsList.get(studentsList.size() - 1);
        assertThat(testStudents.getStudent_name()).isEqualTo(DEFAULT_STUDENT_NAME);
        assertThat(testStudents.getMobile()).isEqualTo(DEFAULT_MOBILE);
        assertThat(testStudents.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testStudents.getStudying_year()).isEqualTo(DEFAULT_STUDYING_YEAR);
        assertThat(testStudents.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testStudents.getCreation_date()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testStudents.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testStudents.getLast_update_date()).isEqualTo(DEFAULT_LAST_UPDATE_DATE);
        assertThat(testStudents.getLast_updated_by()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testStudents.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testStudents.getEmcetecet_rank()).isEqualTo(DEFAULT_EMCETECET_RANK);
        assertThat(testStudents.getCurrent_CGPA()).isEqualTo(DEFAULT_CURRENT_CGPA);
        assertThat(testStudents.getFathers_occupation()).isEqualTo(DEFAULT_FATHERS_OCCUPATION);
        assertThat(testStudents.getMothers_occupation()).isEqualTo(DEFAULT_MOTHERS_OCCUPATION);
        assertThat(testStudents.getSiblings()).isEqualTo(DEFAULT_SIBLINGS);
        assertThat(testStudents.getFamily_income()).isEqualTo(DEFAULT_FAMILY_INCOME);
        assertThat(testStudents.getNative_place()).isEqualTo(DEFAULT_NATIVE_PLACE);
        assertThat(testStudents.getLanguages_known()).isEqualTo(DEFAULT_LANGUAGES_KNOWN);
    }

    @Test
    @Transactional
    public void createStudentsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentsRepository.findAll().size();

        // Create the Students with an existing ID
        students.setId(1L);
        StudentsDTO studentsDTO = studentsMapper.toDto(students);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentsMockMvc.perform(post("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        // Get all the studentsList
        restStudentsMockMvc.perform(get("/api/students?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(students.getId().intValue())))
            .andExpect(jsonPath("$.[*].student_name").value(hasItem(DEFAULT_STUDENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].mobile").value(hasItem(DEFAULT_MOBILE.intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].studying_year").value(hasItem(DEFAULT_STUDYING_YEAR.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].last_update_date").value(hasItem(DEFAULT_LAST_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].last_updated_by").value(hasItem(DEFAULT_LAST_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT.toString())))
            .andExpect(jsonPath("$.[*].emcetecet_rank").value(hasItem(DEFAULT_EMCETECET_RANK.intValue())))
            .andExpect(jsonPath("$.[*].current_CGPA").value(hasItem(DEFAULT_CURRENT_CGPA.doubleValue())))
            .andExpect(jsonPath("$.[*].fathers_occupation").value(hasItem(DEFAULT_FATHERS_OCCUPATION.toString())))
            .andExpect(jsonPath("$.[*].mothers_occupation").value(hasItem(DEFAULT_MOTHERS_OCCUPATION.toString())))
            .andExpect(jsonPath("$.[*].siblings").value(hasItem(DEFAULT_SIBLINGS)))
            .andExpect(jsonPath("$.[*].family_income").value(hasItem(DEFAULT_FAMILY_INCOME.toString())))
            .andExpect(jsonPath("$.[*].native_place").value(hasItem(DEFAULT_NATIVE_PLACE.toString())))
            .andExpect(jsonPath("$.[*].languages_known").value(hasItem(DEFAULT_LANGUAGES_KNOWN.toString())));
    }
    
    @Test
    @Transactional
    public void getStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        // Get the students
        restStudentsMockMvc.perform(get("/api/students/{id}", students.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(students.getId().intValue()))
            .andExpect(jsonPath("$.student_name").value(DEFAULT_STUDENT_NAME.toString()))
            .andExpect(jsonPath("$.mobile").value(DEFAULT_MOBILE.intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.studying_year").value(DEFAULT_STUDYING_YEAR.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.last_update_date").value(DEFAULT_LAST_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.last_updated_by").value(DEFAULT_LAST_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT.toString()))
            .andExpect(jsonPath("$.emcetecet_rank").value(DEFAULT_EMCETECET_RANK.intValue()))
            .andExpect(jsonPath("$.current_CGPA").value(DEFAULT_CURRENT_CGPA.doubleValue()))
            .andExpect(jsonPath("$.fathers_occupation").value(DEFAULT_FATHERS_OCCUPATION.toString()))
            .andExpect(jsonPath("$.mothers_occupation").value(DEFAULT_MOTHERS_OCCUPATION.toString()))
            .andExpect(jsonPath("$.siblings").value(DEFAULT_SIBLINGS))
            .andExpect(jsonPath("$.family_income").value(DEFAULT_FAMILY_INCOME.toString()))
            .andExpect(jsonPath("$.native_place").value(DEFAULT_NATIVE_PLACE.toString()))
            .andExpect(jsonPath("$.languages_known").value(DEFAULT_LANGUAGES_KNOWN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStudents() throws Exception {
        // Get the students
        restStudentsMockMvc.perform(get("/api/students/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();

        // Update the students
        Students updatedStudents = studentsRepository.findById(students.getId()).get();
        // Disconnect from session so that the updates on updatedStudents are not directly saved in db
        em.detach(updatedStudents);
        updatedStudents
            .student_name(UPDATED_STUDENT_NAME)
            .mobile(UPDATED_MOBILE)
            .email(UPDATED_EMAIL)
            .studying_year(UPDATED_STUDYING_YEAR)
            .status(UPDATED_STATUS)
            .creation_date(UPDATED_CREATION_DATE)
            .created_by(UPDATED_CREATED_BY)
            .last_update_date(UPDATED_LAST_UPDATE_DATE)
            .last_updated_by(UPDATED_LAST_UPDATED_BY)
            .department(UPDATED_DEPARTMENT)
            .emcetecet_rank(UPDATED_EMCETECET_RANK)
            .current_CGPA(UPDATED_CURRENT_CGPA)
            .fathers_occupation(UPDATED_FATHERS_OCCUPATION)
            .mothers_occupation(UPDATED_MOTHERS_OCCUPATION)
            .siblings(UPDATED_SIBLINGS)
            .family_income(UPDATED_FAMILY_INCOME)
            .native_place(UPDATED_NATIVE_PLACE)
            .languages_known(UPDATED_LANGUAGES_KNOWN);
        StudentsDTO studentsDTO = studentsMapper.toDto(updatedStudents);

        restStudentsMockMvc.perform(put("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentsDTO)))
            .andExpect(status().isOk());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
        Students testStudents = studentsList.get(studentsList.size() - 1);
        assertThat(testStudents.getStudent_name()).isEqualTo(UPDATED_STUDENT_NAME);
        assertThat(testStudents.getMobile()).isEqualTo(UPDATED_MOBILE);
        assertThat(testStudents.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testStudents.getStudying_year()).isEqualTo(UPDATED_STUDYING_YEAR);
        assertThat(testStudents.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testStudents.getCreation_date()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testStudents.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testStudents.getLast_update_date()).isEqualTo(UPDATED_LAST_UPDATE_DATE);
        assertThat(testStudents.getLast_updated_by()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testStudents.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testStudents.getEmcetecet_rank()).isEqualTo(UPDATED_EMCETECET_RANK);
        assertThat(testStudents.getCurrent_CGPA()).isEqualTo(UPDATED_CURRENT_CGPA);
        assertThat(testStudents.getFathers_occupation()).isEqualTo(UPDATED_FATHERS_OCCUPATION);
        assertThat(testStudents.getMothers_occupation()).isEqualTo(UPDATED_MOTHERS_OCCUPATION);
        assertThat(testStudents.getSiblings()).isEqualTo(UPDATED_SIBLINGS);
        assertThat(testStudents.getFamily_income()).isEqualTo(UPDATED_FAMILY_INCOME);
        assertThat(testStudents.getNative_place()).isEqualTo(UPDATED_NATIVE_PLACE);
        assertThat(testStudents.getLanguages_known()).isEqualTo(UPDATED_LANGUAGES_KNOWN);
    }

    @Test
    @Transactional
    public void updateNonExistingStudents() throws Exception {
        int databaseSizeBeforeUpdate = studentsRepository.findAll().size();

        // Create the Students
        StudentsDTO studentsDTO = studentsMapper.toDto(students);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStudentsMockMvc.perform(put("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Students in the database
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStudents() throws Exception {
        // Initialize the database
        studentsRepository.saveAndFlush(students);

        int databaseSizeBeforeDelete = studentsRepository.findAll().size();

        // Delete the students
        restStudentsMockMvc.perform(delete("/api/students/{id}", students.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Students> studentsList = studentsRepository.findAll();
        assertThat(studentsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Students.class);
        Students students1 = new Students();
        students1.setId(1L);
        Students students2 = new Students();
        students2.setId(students1.getId());
        assertThat(students1).isEqualTo(students2);
        students2.setId(2L);
        assertThat(students1).isNotEqualTo(students2);
        students1.setId(null);
        assertThat(students1).isNotEqualTo(students2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StudentsDTO.class);
        StudentsDTO studentsDTO1 = new StudentsDTO();
        studentsDTO1.setId(1L);
        StudentsDTO studentsDTO2 = new StudentsDTO();
        assertThat(studentsDTO1).isNotEqualTo(studentsDTO2);
        studentsDTO2.setId(studentsDTO1.getId());
        assertThat(studentsDTO1).isEqualTo(studentsDTO2);
        studentsDTO2.setId(2L);
        assertThat(studentsDTO1).isNotEqualTo(studentsDTO2);
        studentsDTO1.setId(null);
        assertThat(studentsDTO1).isNotEqualTo(studentsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(studentsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(studentsMapper.fromId(null)).isNull();
    }
}
