package com.snithik.examapp.web.rest;

import com.snithik.examapp.ExamappApp;

import com.snithik.examapp.domain.Colleges;
import com.snithik.examapp.repository.CollegesRepository;
import com.snithik.examapp.service.CollegesService;
import com.snithik.examapp.service.dto.CollegesDTO;
import com.snithik.examapp.service.mapper.CollegesMapper;
import com.snithik.examapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.snithik.examapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CollegesResource REST controller.
 *
 * @see CollegesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExamappApp.class)
public class CollegesResourceIntTest {

    private static final String DEFAULT_COLLEGE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COLLEGE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COLLEGE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COLLEGE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_AFFILIATED_UNIVERSITY = "AAAAAAAAAA";
    private static final String UPDATED_AFFILIATED_UNIVERSITY = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE = "BBBBBBBBBB";

    private static final Long DEFAULT_CONTACT_NUMBER = 1L;
    private static final Long UPDATED_CONTACT_NUMBER = 2L;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATE_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATE_BY = "BBBBBBBBBB";

    @Autowired
    private CollegesRepository collegesRepository;

    @Autowired
    private CollegesMapper collegesMapper;

    @Autowired
    private CollegesService collegesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCollegesMockMvc;

    private Colleges colleges;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CollegesResource collegesResource = new CollegesResource(collegesService);
        this.restCollegesMockMvc = MockMvcBuilders.standaloneSetup(collegesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Colleges createEntity(EntityManager em) {
        Colleges colleges = new Colleges()
            .college_name(DEFAULT_COLLEGE_NAME)
            .college_code(DEFAULT_COLLEGE_CODE)
            .affiliated_university(DEFAULT_AFFILIATED_UNIVERSITY)
            .address(DEFAULT_ADDRESS)
            .address2(DEFAULT_ADDRESS_2)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .country(DEFAULT_COUNTRY)
            .zip_code(DEFAULT_ZIP_CODE)
            .contact_number(DEFAULT_CONTACT_NUMBER)
            .email(DEFAULT_EMAIL)
            .status(DEFAULT_STATUS)
            .creation_date(DEFAULT_CREATION_DATE)
            .created_by(DEFAULT_CREATED_BY)
            .last_update_date(DEFAULT_LAST_UPDATE_DATE)
            .last_update_by(DEFAULT_LAST_UPDATE_BY);
        return colleges;
    }

    @Before
    public void initTest() {
        colleges = createEntity(em);
    }

    @Test
    @Transactional
    public void createColleges() throws Exception {
        int databaseSizeBeforeCreate = collegesRepository.findAll().size();

        // Create the Colleges
        CollegesDTO collegesDTO = collegesMapper.toDto(colleges);
        restCollegesMockMvc.perform(post("/api/colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(collegesDTO)))
            .andExpect(status().isCreated());

        // Validate the Colleges in the database
        List<Colleges> collegesList = collegesRepository.findAll();
        assertThat(collegesList).hasSize(databaseSizeBeforeCreate + 1);
        Colleges testColleges = collegesList.get(collegesList.size() - 1);
        assertThat(testColleges.getCollege_name()).isEqualTo(DEFAULT_COLLEGE_NAME);
        assertThat(testColleges.getCollege_code()).isEqualTo(DEFAULT_COLLEGE_CODE);
        assertThat(testColleges.getAffiliated_university()).isEqualTo(DEFAULT_AFFILIATED_UNIVERSITY);
        assertThat(testColleges.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testColleges.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testColleges.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testColleges.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testColleges.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testColleges.getZip_code()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testColleges.getContact_number()).isEqualTo(DEFAULT_CONTACT_NUMBER);
        assertThat(testColleges.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testColleges.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testColleges.getCreation_date()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testColleges.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testColleges.getLast_update_date()).isEqualTo(DEFAULT_LAST_UPDATE_DATE);
        assertThat(testColleges.getLast_update_by()).isEqualTo(DEFAULT_LAST_UPDATE_BY);
    }

    @Test
    @Transactional
    public void createCollegesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = collegesRepository.findAll().size();

        // Create the Colleges with an existing ID
        colleges.setId(1L);
        CollegesDTO collegesDTO = collegesMapper.toDto(colleges);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCollegesMockMvc.perform(post("/api/colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(collegesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Colleges in the database
        List<Colleges> collegesList = collegesRepository.findAll();
        assertThat(collegesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllColleges() throws Exception {
        // Initialize the database
        collegesRepository.saveAndFlush(colleges);

        // Get all the collegesList
        restCollegesMockMvc.perform(get("/api/colleges?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(colleges.getId().intValue())))
            .andExpect(jsonPath("$.[*].college_name").value(hasItem(DEFAULT_COLLEGE_NAME.toString())))
            .andExpect(jsonPath("$.[*].college_code").value(hasItem(DEFAULT_COLLEGE_CODE.toString())))
            .andExpect(jsonPath("$.[*].affiliated_university").value(hasItem(DEFAULT_AFFILIATED_UNIVERSITY.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].zip_code").value(hasItem(DEFAULT_ZIP_CODE.toString())))
            .andExpect(jsonPath("$.[*].contact_number").value(hasItem(DEFAULT_CONTACT_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].last_update_date").value(hasItem(DEFAULT_LAST_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].last_update_by").value(hasItem(DEFAULT_LAST_UPDATE_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getColleges() throws Exception {
        // Initialize the database
        collegesRepository.saveAndFlush(colleges);

        // Get the colleges
        restCollegesMockMvc.perform(get("/api/colleges/{id}", colleges.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(colleges.getId().intValue()))
            .andExpect(jsonPath("$.college_name").value(DEFAULT_COLLEGE_NAME.toString()))
            .andExpect(jsonPath("$.college_code").value(DEFAULT_COLLEGE_CODE.toString()))
            .andExpect(jsonPath("$.affiliated_university").value(DEFAULT_AFFILIATED_UNIVERSITY.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.zip_code").value(DEFAULT_ZIP_CODE.toString()))
            .andExpect(jsonPath("$.contact_number").value(DEFAULT_CONTACT_NUMBER.intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.last_update_date").value(DEFAULT_LAST_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.last_update_by").value(DEFAULT_LAST_UPDATE_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingColleges() throws Exception {
        // Get the colleges
        restCollegesMockMvc.perform(get("/api/colleges/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateColleges() throws Exception {
        // Initialize the database
        collegesRepository.saveAndFlush(colleges);

        int databaseSizeBeforeUpdate = collegesRepository.findAll().size();

        // Update the colleges
        Colleges updatedColleges = collegesRepository.findById(colleges.getId()).get();
        // Disconnect from session so that the updates on updatedColleges are not directly saved in db
        em.detach(updatedColleges);
        updatedColleges
            .college_name(UPDATED_COLLEGE_NAME)
            .college_code(UPDATED_COLLEGE_CODE)
            .affiliated_university(UPDATED_AFFILIATED_UNIVERSITY)
            .address(UPDATED_ADDRESS)
            .address2(UPDATED_ADDRESS_2)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .zip_code(UPDATED_ZIP_CODE)
            .contact_number(UPDATED_CONTACT_NUMBER)
            .email(UPDATED_EMAIL)
            .status(UPDATED_STATUS)
            .creation_date(UPDATED_CREATION_DATE)
            .created_by(UPDATED_CREATED_BY)
            .last_update_date(UPDATED_LAST_UPDATE_DATE)
            .last_update_by(UPDATED_LAST_UPDATE_BY);
        CollegesDTO collegesDTO = collegesMapper.toDto(updatedColleges);

        restCollegesMockMvc.perform(put("/api/colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(collegesDTO)))
            .andExpect(status().isOk());

        // Validate the Colleges in the database
        List<Colleges> collegesList = collegesRepository.findAll();
        assertThat(collegesList).hasSize(databaseSizeBeforeUpdate);
        Colleges testColleges = collegesList.get(collegesList.size() - 1);
        assertThat(testColleges.getCollege_name()).isEqualTo(UPDATED_COLLEGE_NAME);
        assertThat(testColleges.getCollege_code()).isEqualTo(UPDATED_COLLEGE_CODE);
        assertThat(testColleges.getAffiliated_university()).isEqualTo(UPDATED_AFFILIATED_UNIVERSITY);
        assertThat(testColleges.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testColleges.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testColleges.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testColleges.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testColleges.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testColleges.getZip_code()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testColleges.getContact_number()).isEqualTo(UPDATED_CONTACT_NUMBER);
        assertThat(testColleges.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testColleges.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testColleges.getCreation_date()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testColleges.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testColleges.getLast_update_date()).isEqualTo(UPDATED_LAST_UPDATE_DATE);
        assertThat(testColleges.getLast_update_by()).isEqualTo(UPDATED_LAST_UPDATE_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingColleges() throws Exception {
        int databaseSizeBeforeUpdate = collegesRepository.findAll().size();

        // Create the Colleges
        CollegesDTO collegesDTO = collegesMapper.toDto(colleges);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCollegesMockMvc.perform(put("/api/colleges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(collegesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Colleges in the database
        List<Colleges> collegesList = collegesRepository.findAll();
        assertThat(collegesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteColleges() throws Exception {
        // Initialize the database
        collegesRepository.saveAndFlush(colleges);

        int databaseSizeBeforeDelete = collegesRepository.findAll().size();

        // Delete the colleges
        restCollegesMockMvc.perform(delete("/api/colleges/{id}", colleges.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Colleges> collegesList = collegesRepository.findAll();
        assertThat(collegesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Colleges.class);
        Colleges colleges1 = new Colleges();
        colleges1.setId(1L);
        Colleges colleges2 = new Colleges();
        colleges2.setId(colleges1.getId());
        assertThat(colleges1).isEqualTo(colleges2);
        colleges2.setId(2L);
        assertThat(colleges1).isNotEqualTo(colleges2);
        colleges1.setId(null);
        assertThat(colleges1).isNotEqualTo(colleges2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CollegesDTO.class);
        CollegesDTO collegesDTO1 = new CollegesDTO();
        collegesDTO1.setId(1L);
        CollegesDTO collegesDTO2 = new CollegesDTO();
        assertThat(collegesDTO1).isNotEqualTo(collegesDTO2);
        collegesDTO2.setId(collegesDTO1.getId());
        assertThat(collegesDTO1).isEqualTo(collegesDTO2);
        collegesDTO2.setId(2L);
        assertThat(collegesDTO1).isNotEqualTo(collegesDTO2);
        collegesDTO1.setId(null);
        assertThat(collegesDTO1).isNotEqualTo(collegesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(collegesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(collegesMapper.fromId(null)).isNull();
    }
}
