package com.snithik.examapp.web.rest;

import com.snithik.examapp.ExamappApp;

import com.snithik.examapp.domain.Question_types;
import com.snithik.examapp.repository.Question_typesRepository;
import com.snithik.examapp.service.Question_typesService;
import com.snithik.examapp.service.dto.Question_typesDTO;
import com.snithik.examapp.service.mapper.Question_typesMapper;
import com.snithik.examapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.snithik.examapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Question_typesResource REST controller.
 *
 * @see Question_typesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExamappApp.class)
public class Question_typesResourceIntTest {

    private static final String DEFAULT_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private Question_typesRepository question_typesRepository;

    @Autowired
    private Question_typesMapper question_typesMapper;

    @Autowired
    private Question_typesService question_typesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restQuestion_typesMockMvc;

    private Question_types question_types;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Question_typesResource question_typesResource = new Question_typesResource(question_typesService);
        this.restQuestion_typesMockMvc = MockMvcBuilders.standaloneSetup(question_typesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Question_types createEntity(EntityManager em) {
        Question_types question_types = new Question_types()
            .type_code(DEFAULT_TYPE_CODE)
            .type_name(DEFAULT_TYPE_NAME)
            .status(DEFAULT_STATUS)
            .creation_date(DEFAULT_CREATION_DATE)
            .created_by(DEFAULT_CREATED_BY)
            .last_update_date(DEFAULT_LAST_UPDATE_DATE)
            .last_updated_by(DEFAULT_LAST_UPDATED_BY);
        return question_types;
    }

    @Before
    public void initTest() {
        question_types = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuestion_types() throws Exception {
        int databaseSizeBeforeCreate = question_typesRepository.findAll().size();

        // Create the Question_types
        Question_typesDTO question_typesDTO = question_typesMapper.toDto(question_types);
        restQuestion_typesMockMvc.perform(post("/api/question-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question_typesDTO)))
            .andExpect(status().isCreated());

        // Validate the Question_types in the database
        List<Question_types> question_typesList = question_typesRepository.findAll();
        assertThat(question_typesList).hasSize(databaseSizeBeforeCreate + 1);
        Question_types testQuestion_types = question_typesList.get(question_typesList.size() - 1);
        assertThat(testQuestion_types.getType_code()).isEqualTo(DEFAULT_TYPE_CODE);
        assertThat(testQuestion_types.getType_name()).isEqualTo(DEFAULT_TYPE_NAME);
        assertThat(testQuestion_types.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testQuestion_types.getCreation_date()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testQuestion_types.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testQuestion_types.getLast_update_date()).isEqualTo(DEFAULT_LAST_UPDATE_DATE);
        assertThat(testQuestion_types.getLast_updated_by()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createQuestion_typesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = question_typesRepository.findAll().size();

        // Create the Question_types with an existing ID
        question_types.setId(1L);
        Question_typesDTO question_typesDTO = question_typesMapper.toDto(question_types);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuestion_typesMockMvc.perform(post("/api/question-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question_typesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Question_types in the database
        List<Question_types> question_typesList = question_typesRepository.findAll();
        assertThat(question_typesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllQuestion_types() throws Exception {
        // Initialize the database
        question_typesRepository.saveAndFlush(question_types);

        // Get all the question_typesList
        restQuestion_typesMockMvc.perform(get("/api/question-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(question_types.getId().intValue())))
            .andExpect(jsonPath("$.[*].type_code").value(hasItem(DEFAULT_TYPE_CODE.toString())))
            .andExpect(jsonPath("$.[*].type_name").value(hasItem(DEFAULT_TYPE_NAME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].last_update_date").value(hasItem(DEFAULT_LAST_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].last_updated_by").value(hasItem(DEFAULT_LAST_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getQuestion_types() throws Exception {
        // Initialize the database
        question_typesRepository.saveAndFlush(question_types);

        // Get the question_types
        restQuestion_typesMockMvc.perform(get("/api/question-types/{id}", question_types.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(question_types.getId().intValue()))
            .andExpect(jsonPath("$.type_code").value(DEFAULT_TYPE_CODE.toString()))
            .andExpect(jsonPath("$.type_name").value(DEFAULT_TYPE_NAME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.last_update_date").value(DEFAULT_LAST_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.last_updated_by").value(DEFAULT_LAST_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingQuestion_types() throws Exception {
        // Get the question_types
        restQuestion_typesMockMvc.perform(get("/api/question-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuestion_types() throws Exception {
        // Initialize the database
        question_typesRepository.saveAndFlush(question_types);

        int databaseSizeBeforeUpdate = question_typesRepository.findAll().size();

        // Update the question_types
        Question_types updatedQuestion_types = question_typesRepository.findById(question_types.getId()).get();
        // Disconnect from session so that the updates on updatedQuestion_types are not directly saved in db
        em.detach(updatedQuestion_types);
        updatedQuestion_types
            .type_code(UPDATED_TYPE_CODE)
            .type_name(UPDATED_TYPE_NAME)
            .status(UPDATED_STATUS)
            .creation_date(UPDATED_CREATION_DATE)
            .created_by(UPDATED_CREATED_BY)
            .last_update_date(UPDATED_LAST_UPDATE_DATE)
            .last_updated_by(UPDATED_LAST_UPDATED_BY);
        Question_typesDTO question_typesDTO = question_typesMapper.toDto(updatedQuestion_types);

        restQuestion_typesMockMvc.perform(put("/api/question-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question_typesDTO)))
            .andExpect(status().isOk());

        // Validate the Question_types in the database
        List<Question_types> question_typesList = question_typesRepository.findAll();
        assertThat(question_typesList).hasSize(databaseSizeBeforeUpdate);
        Question_types testQuestion_types = question_typesList.get(question_typesList.size() - 1);
        assertThat(testQuestion_types.getType_code()).isEqualTo(UPDATED_TYPE_CODE);
        assertThat(testQuestion_types.getType_name()).isEqualTo(UPDATED_TYPE_NAME);
        assertThat(testQuestion_types.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testQuestion_types.getCreation_date()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testQuestion_types.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testQuestion_types.getLast_update_date()).isEqualTo(UPDATED_LAST_UPDATE_DATE);
        assertThat(testQuestion_types.getLast_updated_by()).isEqualTo(UPDATED_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingQuestion_types() throws Exception {
        int databaseSizeBeforeUpdate = question_typesRepository.findAll().size();

        // Create the Question_types
        Question_typesDTO question_typesDTO = question_typesMapper.toDto(question_types);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuestion_typesMockMvc.perform(put("/api/question-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(question_typesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Question_types in the database
        List<Question_types> question_typesList = question_typesRepository.findAll();
        assertThat(question_typesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQuestion_types() throws Exception {
        // Initialize the database
        question_typesRepository.saveAndFlush(question_types);

        int databaseSizeBeforeDelete = question_typesRepository.findAll().size();

        // Delete the question_types
        restQuestion_typesMockMvc.perform(delete("/api/question-types/{id}", question_types.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Question_types> question_typesList = question_typesRepository.findAll();
        assertThat(question_typesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Question_types.class);
        Question_types question_types1 = new Question_types();
        question_types1.setId(1L);
        Question_types question_types2 = new Question_types();
        question_types2.setId(question_types1.getId());
        assertThat(question_types1).isEqualTo(question_types2);
        question_types2.setId(2L);
        assertThat(question_types1).isNotEqualTo(question_types2);
        question_types1.setId(null);
        assertThat(question_types1).isNotEqualTo(question_types2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(Question_typesDTO.class);
        Question_typesDTO question_typesDTO1 = new Question_typesDTO();
        question_typesDTO1.setId(1L);
        Question_typesDTO question_typesDTO2 = new Question_typesDTO();
        assertThat(question_typesDTO1).isNotEqualTo(question_typesDTO2);
        question_typesDTO2.setId(question_typesDTO1.getId());
        assertThat(question_typesDTO1).isEqualTo(question_typesDTO2);
        question_typesDTO2.setId(2L);
        assertThat(question_typesDTO1).isNotEqualTo(question_typesDTO2);
        question_typesDTO1.setId(null);
        assertThat(question_typesDTO1).isNotEqualTo(question_typesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(question_typesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(question_typesMapper.fromId(null)).isNull();
    }
}
