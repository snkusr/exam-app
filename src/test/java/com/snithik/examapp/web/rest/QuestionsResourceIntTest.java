package com.snithik.examapp.web.rest;

import com.snithik.examapp.ExamappApp;

import com.snithik.examapp.domain.Questions;
import com.snithik.examapp.repository.QuestionsRepository;
import com.snithik.examapp.service.QuestionsService;
import com.snithik.examapp.service.dto.QuestionsDTO;
import com.snithik.examapp.service.mapper.QuestionsMapper;
import com.snithik.examapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.snithik.examapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the QuestionsResource REST controller.
 *
 * @see QuestionsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExamappApp.class)
public class QuestionsResourceIntTest {

    private static final String DEFAULT_QUESTION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_QUESTION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWER = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER = "BBBBBBBBBB";

    private static final String DEFAULT_OPTION_1 = "AAAAAAAAAA";
    private static final String UPDATED_OPTION_1 = "BBBBBBBBBB";

    private static final String DEFAULT_OPTION_2 = "AAAAAAAAAA";
    private static final String UPDATED_OPTION_2 = "BBBBBBBBBB";

    private static final String DEFAULT_OPTION_3 = "AAAAAAAAAA";
    private static final String UPDATED_OPTION_3 = "BBBBBBBBBB";

    private static final String DEFAULT_OPTION_4 = "AAAAAAAAAA";
    private static final String UPDATED_OPTION_4 = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private QuestionsRepository questionsRepository;

    @Autowired
    private QuestionsMapper questionsMapper;

    @Autowired
    private QuestionsService questionsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restQuestionsMockMvc;

    private Questions questions;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final QuestionsResource questionsResource = new QuestionsResource(questionsService);
        this.restQuestionsMockMvc = MockMvcBuilders.standaloneSetup(questionsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Questions createEntity(EntityManager em) {
        Questions questions = new Questions()
            .question_name(DEFAULT_QUESTION_NAME)
            .answer(DEFAULT_ANSWER)
            .option1(DEFAULT_OPTION_1)
            .option2(DEFAULT_OPTION_2)
            .option3(DEFAULT_OPTION_3)
            .option4(DEFAULT_OPTION_4)
            .status(DEFAULT_STATUS)
            .creation_date(DEFAULT_CREATION_DATE)
            .created_by(DEFAULT_CREATED_BY)
            .last_update_date(DEFAULT_LAST_UPDATE_DATE)
            .last_updated_by(DEFAULT_LAST_UPDATED_BY);
        return questions;
    }

    @Before
    public void initTest() {
        questions = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuestions() throws Exception {
        int databaseSizeBeforeCreate = questionsRepository.findAll().size();

        // Create the Questions
        QuestionsDTO questionsDTO = questionsMapper.toDto(questions);
        restQuestionsMockMvc.perform(post("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionsDTO)))
            .andExpect(status().isCreated());

        // Validate the Questions in the database
        List<Questions> questionsList = questionsRepository.findAll();
        assertThat(questionsList).hasSize(databaseSizeBeforeCreate + 1);
        Questions testQuestions = questionsList.get(questionsList.size() - 1);
        assertThat(testQuestions.getQuestion_name()).isEqualTo(DEFAULT_QUESTION_NAME);
        assertThat(testQuestions.getAnswer()).isEqualTo(DEFAULT_ANSWER);
        assertThat(testQuestions.getOption1()).isEqualTo(DEFAULT_OPTION_1);
        assertThat(testQuestions.getOption2()).isEqualTo(DEFAULT_OPTION_2);
        assertThat(testQuestions.getOption3()).isEqualTo(DEFAULT_OPTION_3);
        assertThat(testQuestions.getOption4()).isEqualTo(DEFAULT_OPTION_4);
        assertThat(testQuestions.isStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testQuestions.getCreation_date()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testQuestions.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testQuestions.getLast_update_date()).isEqualTo(DEFAULT_LAST_UPDATE_DATE);
        assertThat(testQuestions.getLast_updated_by()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createQuestionsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = questionsRepository.findAll().size();

        // Create the Questions with an existing ID
        questions.setId(1L);
        QuestionsDTO questionsDTO = questionsMapper.toDto(questions);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuestionsMockMvc.perform(post("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Questions in the database
        List<Questions> questionsList = questionsRepository.findAll();
        assertThat(questionsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllQuestions() throws Exception {
        // Initialize the database
        questionsRepository.saveAndFlush(questions);

        // Get all the questionsList
        restQuestionsMockMvc.perform(get("/api/questions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(questions.getId().intValue())))
            .andExpect(jsonPath("$.[*].question_name").value(hasItem(DEFAULT_QUESTION_NAME.toString())))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER.toString())))
            .andExpect(jsonPath("$.[*].option1").value(hasItem(DEFAULT_OPTION_1.toString())))
            .andExpect(jsonPath("$.[*].option2").value(hasItem(DEFAULT_OPTION_2.toString())))
            .andExpect(jsonPath("$.[*].option3").value(hasItem(DEFAULT_OPTION_3.toString())))
            .andExpect(jsonPath("$.[*].option4").value(hasItem(DEFAULT_OPTION_4.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].last_update_date").value(hasItem(DEFAULT_LAST_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].last_updated_by").value(hasItem(DEFAULT_LAST_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getQuestions() throws Exception {
        // Initialize the database
        questionsRepository.saveAndFlush(questions);

        // Get the questions
        restQuestionsMockMvc.perform(get("/api/questions/{id}", questions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(questions.getId().intValue()))
            .andExpect(jsonPath("$.question_name").value(DEFAULT_QUESTION_NAME.toString()))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER.toString()))
            .andExpect(jsonPath("$.option1").value(DEFAULT_OPTION_1.toString()))
            .andExpect(jsonPath("$.option2").value(DEFAULT_OPTION_2.toString()))
            .andExpect(jsonPath("$.option3").value(DEFAULT_OPTION_3.toString()))
            .andExpect(jsonPath("$.option4").value(DEFAULT_OPTION_4.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.last_update_date").value(DEFAULT_LAST_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.last_updated_by").value(DEFAULT_LAST_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingQuestions() throws Exception {
        // Get the questions
        restQuestionsMockMvc.perform(get("/api/questions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuestions() throws Exception {
        // Initialize the database
        questionsRepository.saveAndFlush(questions);

        int databaseSizeBeforeUpdate = questionsRepository.findAll().size();

        // Update the questions
        Questions updatedQuestions = questionsRepository.findById(questions.getId()).get();
        // Disconnect from session so that the updates on updatedQuestions are not directly saved in db
        em.detach(updatedQuestions);
        updatedQuestions
            .question_name(UPDATED_QUESTION_NAME)
            .answer(UPDATED_ANSWER)
            .option1(UPDATED_OPTION_1)
            .option2(UPDATED_OPTION_2)
            .option3(UPDATED_OPTION_3)
            .option4(UPDATED_OPTION_4)
            .status(UPDATED_STATUS)
            .creation_date(UPDATED_CREATION_DATE)
            .created_by(UPDATED_CREATED_BY)
            .last_update_date(UPDATED_LAST_UPDATE_DATE)
            .last_updated_by(UPDATED_LAST_UPDATED_BY);
        QuestionsDTO questionsDTO = questionsMapper.toDto(updatedQuestions);

        restQuestionsMockMvc.perform(put("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionsDTO)))
            .andExpect(status().isOk());

        // Validate the Questions in the database
        List<Questions> questionsList = questionsRepository.findAll();
        assertThat(questionsList).hasSize(databaseSizeBeforeUpdate);
        Questions testQuestions = questionsList.get(questionsList.size() - 1);
        assertThat(testQuestions.getQuestion_name()).isEqualTo(UPDATED_QUESTION_NAME);
        assertThat(testQuestions.getAnswer()).isEqualTo(UPDATED_ANSWER);
        assertThat(testQuestions.getOption1()).isEqualTo(UPDATED_OPTION_1);
        assertThat(testQuestions.getOption2()).isEqualTo(UPDATED_OPTION_2);
        assertThat(testQuestions.getOption3()).isEqualTo(UPDATED_OPTION_3);
        assertThat(testQuestions.getOption4()).isEqualTo(UPDATED_OPTION_4);
        assertThat(testQuestions.isStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testQuestions.getCreation_date()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testQuestions.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testQuestions.getLast_update_date()).isEqualTo(UPDATED_LAST_UPDATE_DATE);
        assertThat(testQuestions.getLast_updated_by()).isEqualTo(UPDATED_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingQuestions() throws Exception {
        int databaseSizeBeforeUpdate = questionsRepository.findAll().size();

        // Create the Questions
        QuestionsDTO questionsDTO = questionsMapper.toDto(questions);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restQuestionsMockMvc.perform(put("/api/questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Questions in the database
        List<Questions> questionsList = questionsRepository.findAll();
        assertThat(questionsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteQuestions() throws Exception {
        // Initialize the database
        questionsRepository.saveAndFlush(questions);

        int databaseSizeBeforeDelete = questionsRepository.findAll().size();

        // Delete the questions
        restQuestionsMockMvc.perform(delete("/api/questions/{id}", questions.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Questions> questionsList = questionsRepository.findAll();
        assertThat(questionsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Questions.class);
        Questions questions1 = new Questions();
        questions1.setId(1L);
        Questions questions2 = new Questions();
        questions2.setId(questions1.getId());
        assertThat(questions1).isEqualTo(questions2);
        questions2.setId(2L);
        assertThat(questions1).isNotEqualTo(questions2);
        questions1.setId(null);
        assertThat(questions1).isNotEqualTo(questions2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(QuestionsDTO.class);
        QuestionsDTO questionsDTO1 = new QuestionsDTO();
        questionsDTO1.setId(1L);
        QuestionsDTO questionsDTO2 = new QuestionsDTO();
        assertThat(questionsDTO1).isNotEqualTo(questionsDTO2);
        questionsDTO2.setId(questionsDTO1.getId());
        assertThat(questionsDTO1).isEqualTo(questionsDTO2);
        questionsDTO2.setId(2L);
        assertThat(questionsDTO1).isNotEqualTo(questionsDTO2);
        questionsDTO1.setId(null);
        assertThat(questionsDTO1).isNotEqualTo(questionsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(questionsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(questionsMapper.fromId(null)).isNull();
    }
}
