package com.snithik.examapp.web.rest;

import com.snithik.examapp.ExamappApp;

import com.snithik.examapp.domain.Exam_paper;
import com.snithik.examapp.repository.Exam_paperRepository;
import com.snithik.examapp.service.Exam_paperService;
import com.snithik.examapp.service.dto.Exam_paperDTO;
import com.snithik.examapp.service.mapper.Exam_paperMapper;
import com.snithik.examapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.snithik.examapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Exam_paperResource REST controller.
 *
 * @see Exam_paperResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExamappApp.class)
public class Exam_paperResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private Exam_paperRepository exam_paperRepository;

    @Autowired
    private Exam_paperMapper exam_paperMapper;

    @Autowired
    private Exam_paperService exam_paperService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExam_paperMockMvc;

    private Exam_paper exam_paper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final Exam_paperResource exam_paperResource = new Exam_paperResource(exam_paperService);
        this.restExam_paperMockMvc = MockMvcBuilders.standaloneSetup(exam_paperResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Exam_paper createEntity(EntityManager em) {
        Exam_paper exam_paper = new Exam_paper()
            .name(DEFAULT_NAME)
            .creation_date(DEFAULT_CREATION_DATE)
            .created_by(DEFAULT_CREATED_BY)
            .last_update_date(DEFAULT_LAST_UPDATE_DATE)
            .last_updated_by(DEFAULT_LAST_UPDATED_BY);
        return exam_paper;
    }

    @Before
    public void initTest() {
        exam_paper = createEntity(em);
    }

    @Test
    @Transactional
    public void createExam_paper() throws Exception {
        int databaseSizeBeforeCreate = exam_paperRepository.findAll().size();

        // Create the Exam_paper
        Exam_paperDTO exam_paperDTO = exam_paperMapper.toDto(exam_paper);
        restExam_paperMockMvc.perform(post("/api/exam-papers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exam_paperDTO)))
            .andExpect(status().isCreated());

        // Validate the Exam_paper in the database
        List<Exam_paper> exam_paperList = exam_paperRepository.findAll();
        assertThat(exam_paperList).hasSize(databaseSizeBeforeCreate + 1);
        Exam_paper testExam_paper = exam_paperList.get(exam_paperList.size() - 1);
        assertThat(testExam_paper.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testExam_paper.getCreation_date()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testExam_paper.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testExam_paper.getLast_update_date()).isEqualTo(DEFAULT_LAST_UPDATE_DATE);
        assertThat(testExam_paper.getLast_updated_by()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void createExam_paperWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = exam_paperRepository.findAll().size();

        // Create the Exam_paper with an existing ID
        exam_paper.setId(1L);
        Exam_paperDTO exam_paperDTO = exam_paperMapper.toDto(exam_paper);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExam_paperMockMvc.perform(post("/api/exam-papers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exam_paperDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Exam_paper in the database
        List<Exam_paper> exam_paperList = exam_paperRepository.findAll();
        assertThat(exam_paperList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllExam_papers() throws Exception {
        // Initialize the database
        exam_paperRepository.saveAndFlush(exam_paper);

        // Get all the exam_paperList
        restExam_paperMockMvc.perform(get("/api/exam-papers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exam_paper.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].creation_date").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].last_update_date").value(hasItem(DEFAULT_LAST_UPDATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].last_updated_by").value(hasItem(DEFAULT_LAST_UPDATED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getExam_paper() throws Exception {
        // Initialize the database
        exam_paperRepository.saveAndFlush(exam_paper);

        // Get the exam_paper
        restExam_paperMockMvc.perform(get("/api/exam-papers/{id}", exam_paper.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(exam_paper.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.creation_date").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.last_update_date").value(DEFAULT_LAST_UPDATE_DATE.toString()))
            .andExpect(jsonPath("$.last_updated_by").value(DEFAULT_LAST_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExam_paper() throws Exception {
        // Get the exam_paper
        restExam_paperMockMvc.perform(get("/api/exam-papers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExam_paper() throws Exception {
        // Initialize the database
        exam_paperRepository.saveAndFlush(exam_paper);

        int databaseSizeBeforeUpdate = exam_paperRepository.findAll().size();

        // Update the exam_paper
        Exam_paper updatedExam_paper = exam_paperRepository.findById(exam_paper.getId()).get();
        // Disconnect from session so that the updates on updatedExam_paper are not directly saved in db
        em.detach(updatedExam_paper);
        updatedExam_paper
            .name(UPDATED_NAME)
            .creation_date(UPDATED_CREATION_DATE)
            .created_by(UPDATED_CREATED_BY)
            .last_update_date(UPDATED_LAST_UPDATE_DATE)
            .last_updated_by(UPDATED_LAST_UPDATED_BY);
        Exam_paperDTO exam_paperDTO = exam_paperMapper.toDto(updatedExam_paper);

        restExam_paperMockMvc.perform(put("/api/exam-papers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exam_paperDTO)))
            .andExpect(status().isOk());

        // Validate the Exam_paper in the database
        List<Exam_paper> exam_paperList = exam_paperRepository.findAll();
        assertThat(exam_paperList).hasSize(databaseSizeBeforeUpdate);
        Exam_paper testExam_paper = exam_paperList.get(exam_paperList.size() - 1);
        assertThat(testExam_paper.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testExam_paper.getCreation_date()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testExam_paper.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testExam_paper.getLast_update_date()).isEqualTo(UPDATED_LAST_UPDATE_DATE);
        assertThat(testExam_paper.getLast_updated_by()).isEqualTo(UPDATED_LAST_UPDATED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingExam_paper() throws Exception {
        int databaseSizeBeforeUpdate = exam_paperRepository.findAll().size();

        // Create the Exam_paper
        Exam_paperDTO exam_paperDTO = exam_paperMapper.toDto(exam_paper);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExam_paperMockMvc.perform(put("/api/exam-papers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(exam_paperDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Exam_paper in the database
        List<Exam_paper> exam_paperList = exam_paperRepository.findAll();
        assertThat(exam_paperList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteExam_paper() throws Exception {
        // Initialize the database
        exam_paperRepository.saveAndFlush(exam_paper);

        int databaseSizeBeforeDelete = exam_paperRepository.findAll().size();

        // Delete the exam_paper
        restExam_paperMockMvc.perform(delete("/api/exam-papers/{id}", exam_paper.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Exam_paper> exam_paperList = exam_paperRepository.findAll();
        assertThat(exam_paperList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Exam_paper.class);
        Exam_paper exam_paper1 = new Exam_paper();
        exam_paper1.setId(1L);
        Exam_paper exam_paper2 = new Exam_paper();
        exam_paper2.setId(exam_paper1.getId());
        assertThat(exam_paper1).isEqualTo(exam_paper2);
        exam_paper2.setId(2L);
        assertThat(exam_paper1).isNotEqualTo(exam_paper2);
        exam_paper1.setId(null);
        assertThat(exam_paper1).isNotEqualTo(exam_paper2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(Exam_paperDTO.class);
        Exam_paperDTO exam_paperDTO1 = new Exam_paperDTO();
        exam_paperDTO1.setId(1L);
        Exam_paperDTO exam_paperDTO2 = new Exam_paperDTO();
        assertThat(exam_paperDTO1).isNotEqualTo(exam_paperDTO2);
        exam_paperDTO2.setId(exam_paperDTO1.getId());
        assertThat(exam_paperDTO1).isEqualTo(exam_paperDTO2);
        exam_paperDTO2.setId(2L);
        assertThat(exam_paperDTO1).isNotEqualTo(exam_paperDTO2);
        exam_paperDTO1.setId(null);
        assertThat(exam_paperDTO1).isNotEqualTo(exam_paperDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(exam_paperMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(exam_paperMapper.fromId(null)).isNull();
    }
}
